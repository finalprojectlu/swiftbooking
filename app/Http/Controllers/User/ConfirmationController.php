<?php

namespace App\Http\Controllers\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Organization;
use App\Category;
use App\Schedule;
use App\Booking;
use App\Booked_item;
use App\Booked_user;
use App\Section_attribute_item;
use Auth;


class ConfirmationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function confirmation()
    {
        $user_id=Auth::user()->id;
        $booking=DB::table('bookings')
        ->join('booked_users','booked_users.booking_id','=','bookings.id')
        ->join('booked_items','booked_items.booking_id','=','bookings.id')
        ->join('section_attribute_items','booked_items.booked_item_id','=','section_attribute_items.id')
        ->join('organizations','organizations.id','=','section_attribute_items.organization_id')
        ->select('bookings.*','bookings.status AS bookingStatus','bookings.id AS bookingID','booked_users.*','booked_items.*','section_attribute_items.*','organizations.organization_name')
        ->where(['bookings.owner_id'=>$user_id])
        ->orderBy('bookings.id', 'desc')
        ->get();
        return view('user.confirmation',['bookings'=>$booking]); 
    }
    public function enable($id)
    {
        $booking=new Booking;
        $booking->status=1;
        $data=array(
            'status' => 1 
                );

        Booking::where('id',$id)->update($data);
        $booking->update();
        return redirect('/organization/confirmation')->with(['response'=>'Confirmation Updated'] ); ; 
    }
    public function disable($id)
    {
        $booking=new Booking;
        $booking->status=0;
        $data=array(
            'status' => 0 
                );

        Booking::where('id',$id)->update($data);
        $booking->update();
        return redirect('/organization/confirmation')->with(['response'=>'Confirmation Updatedd'] ); ; 
    }
}
