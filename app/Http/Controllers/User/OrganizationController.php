<?php

namespace App\Http\Controllers\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Organization;
use App\Category;
use App\Schedule;
use App\Section_attribute_item;
use Auth;

class OrganizationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
 
    }
    public function organizations()
    {
        $user_id=Auth::user()->id;
        $organization=DB::table('organizations')
        ->join('categories','organizations.organization_category','=','categories.id')
        ->select('organizations.*','categories.category_title','categories.category_icon')
        ->where(['organizations.user_id'=>$user_id,'organizations.status'=>1])
        ->get();
        return view('user.organizations',['organizations'=>$organization]); 
    }
    public function addorganization(Request $request)
    {
        $category=Category::get();
        return view('user.organization.addorganization',['categories'=>$category]); 
    }
    public function addedorganization(Request $request)
    {
        $user_id=Auth::user()->id;
        $this->validate($request,
            [
                'organization_name'=> 'required',
                'organization_category'=> 'required'
            ]
            );
        $organization=new Organization;
        $organization->user_id=$user_id;
        $organization->organization_name=$request->input('organization_name');
        $url=null;
        if(Input::hasFile('organization_cover'))
            {
                $file=Input::file('organization_cover');
                $file->move(public_path().'/organizationcover/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationcover/'. $file->getClientOriginalName();

            }
        $organization->organization_cover= $url;
        $organization->organization_link=$request->input('organization_link');
        $organization->organization_category=$request->input('organization_category');
        $organization->organization_info=$request->input('organization_info');
        $organization->organization_address=$request->input('organization_address');
        $organization->organization_contact=$request->input('organization_contact');
        $organization->organization_map=$request->input('organization_map');
        $organization->save();
        return redirect('organization/view/'.$organization->id)->with(['response'=>'  Added Successfully'] ); ; 
    }
    public function organizationview($id)
    {
        $organization=Organization::find($id);
        $category=Category::find($organization->organization_category);
        if($category->category_type==3)
        {
            return view('user.organization.organizationviewrent',['organization'=>$organization,'category'=>$category]); 
        }
        if($category->category_type==2)
        {
            return view('user.organization.organizationviewseat',['organization'=>$organization,'category'=>$category]); 
        }
        else
        return view('user.organization.organizationviewticket',['organization'=>$organization,'category'=>$category]); 
    }
    public function finishorganization(Request $request)
    {
        
        $user_id=Auth::user()->id;
        $organization_id=$request->input('organization_id');
        $sunday=$monday=$tuesday=$wednesday=$thursday=$friday=$saturday=0;
        if($request->input('sunday'))
            $sunday=1;
        if($request->input('monday'))
            $monday=1;
        if($request->input('tuesday'))
            $tuesday=1;
        if($request->input('wednesday'))
            $wednesday=1;
        if($request->input('thursday'))
            $thursday=1;
        if($request->input('friday'))
            $friday=1;
        if($request->input('saturday'))
            $saturday=1;

        $schedule=new Schedule;
        $schedule->organization_id=$organization_id;
        $schedule->booking_limit=$request->input('booking_limit');
        $schedule->starting_time=$request->input('starting_time');
        $schedule->ending_time=$request->input('ending_time');
        $schedule->sunday=$request->input('sunday');
        $schedule->monday=$request->input('monday');
        $schedule->tuesday=$request->input('tuesday');
        $schedule->wednesday=$request->input('wednesday');
        $schedule->thursday=$request->input('thursday');
        $schedule->friday=$request->input('friday');
        $schedule->saturday=$request->input('saturday');
        $schedule->save();

        $totalitem=count($request->input('item_title'));
        for($i = 0; $i < $totalitem; $i++){

            $section=new Section_attribute_item;

            $section->organization_id=$organization_id;
            $section->item_title=$request->input('item_title')[$i];
            $url="";
            if(Input::hasFile('item_image1')[$i])
            {
                $file=Input::file('item_image1')[$i];
                $file->move(public_path().'/organizationalbum/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationalbum/'. $file->getClientOriginalName();

            }
            $section->item_image1=$url;
            if(Input::hasFile('item_image2')[$i])
            {
                $file=Input::file('item_image2')[$i];
                $file->move(public_path().'/organizationalbum/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationalbum/'. $file->getClientOriginalName();

            }
            $section->item_image2= $url;
            $section->item_link=$request->input('item_link')[$i];
            $section->item_price=$request->input('item_price')[$i];
            $section->item_price_type=$request->input('item_price_type')[$i];
            $section->item_info=$request->input('item_info')[$i];
            $section->item_features=$request->input('item_features')[$i];
            $section->item_status=$request->input('item_status')[$i];
            $section->item_rating=$request->input('item_rating')[$i];
            $section->item_discount=$request->input('item_discount')[$i];
            $section->section_attribute_id=$request->input('section_attribute_id')[$i];
            $section->limit=$request->input('limit')[$i];
            $section->save();
    
        }



        return redirect('/organization')->with(['response'=>'  Added Successfully'] );
    }
    public function finishorganizationseat(Request $request)
    {
        $user_id=Auth::user()->id;
        $organization_id=$request->input('organization_id');
        $sunday=$monday=$tuesday=$wednesday=$thursday=$friday=$saturday=0;
        if($request->input('sunday'))
            $sunday=1;
        if($request->input('monday'))
            $monday=1;
        if($request->input('tuesday'))
            $tuesday=1;
        if($request->input('wednesday'))
            $wednesday=1;
        if($request->input('thursday'))
            $thursday=1;
        if($request->input('friday'))
            $friday=1;
        if($request->input('saturday'))
            $saturday=1;
        $url=URL::to("/").'/organizationalbum/map.jpg';
        if(Input::hasFile('organization_graphical'))
        {
            $file=Input::file('organization_graphical');
            $file->move(public_path().'/organizationmap/', $file->getClientOriginalName());
            $url=URL::to("/").'/organizationmap/'. $file->getClientOriginalName();

        }
        $data=array(
            'organization_graphical' => $url
                );
        Organization::where('id',$organization_id)->update($data);

        $schedule=new Schedule;
        $schedule->organization_id=$organization_id;
        $schedule->booking_limit=$request->input('booking_limit');
        $schedule->starting_time=$request->input('starting_time');
        $schedule->ending_time=$request->input('ending_time');
        $schedule->sunday=$request->input('sunday');
        $schedule->monday=$request->input('monday');
        $schedule->tuesday=$request->input('tuesday');
        $schedule->wednesday=$request->input('wednesday');
        $schedule->thursday=$request->input('thursday');
        $schedule->friday=$request->input('friday');
        $schedule->saturday=$request->input('saturday');
        $schedule->save();

        $totalitem=count($request->input('item_title'));
        for($i = 0; $i < $totalitem; $i++){

            $section=new Section_attribute_item;

            $section->organization_id=$organization_id;
            $section->item_title=$request->input('item_title')[$i];
            $url="lol";
            if(isset($request->file('item_image1')[$i]))
            {
                $file=$request->file('item_image1')[$i];
                $file->move(public_path().'/organizationalbum/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationalbum/'. $file->getClientOriginalName();

            }
            $section->item_image1=$url;
            $url="lol";
            if(isset($request->file('item_image2')[$i]))
            {
                $file=Input::file('item_image2')[$i];
                $file->move(public_path().'/organizationalbum/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationalbum/'. $file->getClientOriginalName();

            }
            $section->item_image2= $url;
            $section->item_link=$request->input('item_link')[$i];
            $section->item_price=$request->input('item_price')[$i];
            $section->item_price_type=$request->input('item_price_type')[$i];
            $section->item_info=$request->input('item_info')[$i];
            $section->item_features=$request->input('item_features')[$i];
            $section->item_status=$request->input('item_status')[$i];
            $section->item_rating=$request->input('item_rating')[$i];
            $section->item_discount=$request->input('item_discount')[$i];
            $section->section_attribute_id=$request->input('section_attribute_id')[$i];
            $section->limit=$request->input('limit')[$i];
            $section->save();
    
        }



        return redirect('/organization')->with(['response'=>'  Added Successfully'] ); ; 
    }
    public function organizationedit($id)
    {
        $organization=DB::table('organizations')
        ->join('schedules','schedules.organization_id','=','organizations.id')
        ->join('categories','categories.id','=','organizations.organization_category')
        ->select('schedules.*',
        'categories.category_title','categories.category_icon',
        'organizations.id AS org_id','schedules.id AS schedule_id'
        ,'organizations.user_id AS userid','organizations.organization_name AS organization_name','organizations.organization_graphical AS organization_graphical'
        ,'organizations.organization_cover AS organization_cover','organizations.organization_link AS organization_link','organizations.organization_category AS organization_category'
        ,'organizations.organization_info AS organization_info','organizations.organization_address AS organization_address','organizations.organization_map AS organization_map')
        ->where(['organizations.id'=>$id])
        ->first();
        $category=Category::find($organization->organization_category);

        $schedule=Schedule::where('organization_id','=',$id)
        ->first();

        $items=Section_attribute_item::where(['organization_id'=>$id])
        ->get();
        
        if($category->category_type==2)
            return view('user.organization.organizationeditseat',['organization'=>$organization,'items'=>$items,'schedule'=>$schedule]); 
        elseif($category->category_type==3)
        return view('user.organization.organizationeditrent',['organization'=>$organization,'items'=>$items,'schedule'=>$schedule]); 
        else
            return view('user.organization.organizationedit',['organization'=>$organization,'items'=>$items,'schedule'=>$schedule]); 

    }
    public function organizationupdate(Request $request)
    {
        //dd($request->input('schedule_id'));
        $org_id=$request->input('organization_id');
        $url=$request->input('organization_cover');
        $url2=$request->input('organization_graphical');
        $organization=new Organization;
        if(Input::hasFile('organization_cover'))
            {
                $file=Input::file('organization_cover');
                $file->move(public_path().'/organizationcover/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationcover/'. $file->getClientOriginalName();

            }
            if(Input::hasFile('organization_graphical'))
            {
                $file=Input::file('organization_graphical');
                $file->move(public_path().'/organizationcover/', $file->getClientOriginalName());
                $url2=URL::to("/").'/organizationcover/'. $file->getClientOriginalName();

            }
        $organization->organization_cover= $url;
        $organization->organization_graphical= $url2;
        $organization->organization_name=$request->input('organization_name');
        $organization->organization_cover=$request->input('organization_cover');
        $organization->organization_link=$request->input('organization_link');
        $organization->organization_info=$request->input('organization_info');
        $organization->organization_address=$request->input('organization_address');
        $organization->organization_contact=$request->input('organization_contact');
        $organization->organization_map=$request->input('organization_map');

        $schedule=new Schedule;
        $schedule->booking_limit=$request->input('booking_limit');
        $schedule->starting_time=$request->input('starting_time');
        $schedule->ending_time=$request->input('ending_time');
        $schedule->sunday=$request->input('sunday');
        $schedule->monday=$request->input('monday');
        $schedule->tuesday=$request->input('tuesday');
        $schedule->wednesday=$request->input('wednesday');
        $schedule->thursday=$request->input('thursday');
        $schedule->friday=$request->input('friday');
        $schedule->saturday=$request->input('saturday');
        $dataschedule=array(
            'booking_limit' =>$request->input('booking_limit'),
            'ending_time' =>$request->input('ending_time'),
            'sunday' =>$request->input('sunday'),
            'monday' =>$request->input('monday'),
            'tuesday' =>$request->input('tuesday'),
            'wednesday' =>$request->input('wednesday'),
            'thursday' =>$request->input('thursday'),
            'friday' =>$request->input('friday'),
            'saturday' =>$request->input('saturday'),
                );
        Schedule::where('id',$request->input('schedule_id'))->update($dataschedule);
        $schedule->update();
        $dataorg=array(
            'organization_name' =>$request->input('organization_name'),
            'organization_cover' =>$url,
            'organization_graphical' =>$url2,
            'organization_link' =>$request->input('organization_link'),
            'organization_info' =>$request->input('organization_info'),
            'organization_address' =>$request->input('organization_address'),
            'organization_contact' =>$request->input('organization_contact'),
                );
        
        Organization::where('id',$org_id)->update($dataorg);
        $organization->update();
                

        return redirect('/organization/edit/'.$org_id)->with(['response'=>'Item is Enabled'] );

    }
    
    public function itemenable($id)
    {
        $org_id=Section_attribute_item::find($id)->organization_id;
        $item_update=new Section_attribute_item;
        $item_update->status=1;
        $data=array(
            'status' => 1 
                );

        Section_attribute_item::where('id',$id)->update($data);
        $item_update->update();
        return redirect('/organization/edit/'.$org_id)->with(['response'=>'Item is Enabled'] ); ; 
    }
    public function itemdisable($id)
    {
        $org_id=Section_attribute_item::find($id)->organization_id;
        $item_update=new Section_attribute_item;
        $item_update->status=0;
        $data=array(
            'status' => 0 
                );

        Section_attribute_item::where('id',$id)->update($data);
        $item_update->update();
        return redirect('/organization/edit/'.$org_id)->with(['response'=>'Item is Disabled'] ); ; 
    }
    public function itemdelete($id)
    {
        $org_id=Section_attribute_item::find($id)->organization_id;
        Section_attribute_item::where('id',$id)->delete();

        return redirect('/organization/edit/'.$org_id)->with(['response'=>'Item Has Deleted Successfully'] ); ; 
    }
    public function itemedit($id)
    {
        
        Section_attribute_item::where('id',$id)->delete();

        return redirect('/dashboard/category/list')->with(['categories'=>$category,'response'=>'Category Has Deleted Successfully'] ); ; 
    }
    public function addsection(Request $request)
    {
        $user_id=Auth::user()->id;
        $organization_id=$request->input('organization_id');
        $section=new Section_attribute_item;
        $section->organization_id=$request->input('organization_id');
        $section->item_title=$request->input('item_title');
        $url="";
        if(Input::hasFile('item_image1'))
            {
                $file=$request->file('item_image1');
                $file->move(public_path().'/organizationalbum/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationalbum/'. $file->getClientOriginalName();

            }
        $section->item_image1=$url;
        $url="";
        if(Input::hasFile('item_image2'))
            {
                $file=Input::file('item_image2');
                $file->move(public_path().'/organizationalbum/', $file->getClientOriginalName());
                $url=URL::to("/").'/organizationalbum/'. $file->getClientOriginalName();

            }
        $section->item_image2= $url;
        $section->item_link=$request->input('item_link');
        $section->item_price=$request->input('item_price');
        $section->item_price_type=$request->input('item_price_type');
        $section->item_info=$request->input('item_info');
        $section->item_features=$request->input('item_features');
        $section->item_status=$request->input('item_status');
        $section->item_rating=$request->input('item_rating');
        $section->item_discount=$request->input('item_discount');
        $section->section_attribute_id=$request->input('section_attribute_id');
        $section->limit=$request->input('limit');
        $section->save();
        return redirect('organization/edit/'.$organization_id)->with(['response'=>'  Added Successfully'] ); ; 
    }
}
