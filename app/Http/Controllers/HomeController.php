<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }
    public function search(Request $request)
    {
        $search=$request->input('search');
        $organization=DB::table('organizations')
        ->join('users','organizations.user_id','=','users.id')
        ->select('organizations.*','users.*','organizations.id AS org_id')
        ->where('organization_name','LIKE','%'.$search.'%')
        ->where('organizations.status','=',1)
        ->paginate(15);

        return view('guest.organizationlist')->with(['organizations'=>$organization,'response'=>'Search Result of " '.$search.' "'] );
    }
}
