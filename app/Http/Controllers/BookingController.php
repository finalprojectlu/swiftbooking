<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Booking;
use App\Booked_user;
use App\Booked_item;
use App\Booking_log;

class BookingController extends Controller
{
    public function booking_store(Request $request)
    {
        $org_id=$request->input('organization_id');
        $owner_id=$request->input('owner_id');
        $this->validate($request,
            [
                'booking_date'=> 'required',
                'booked_item_id'=> 'required',
                'booked_user_phone'=> 'required',
                'booked_user_name'=> 'required',
            ]
            );
        $booking=new Booking;
        $booking->organization_id=$org_id;
        $booking->owner_id=$owner_id;
        $booking->booking_time=$request->input('booking_time');
        $booking->booking_date=$request->input('booking_date');
        $booking->booking_guests=$request->input('booking_guests');
        $booking->status=2;
        $booking->save();
        $booking_id=$booking->id;
        $booking_item=new Booked_item;
        $booking_item->booking_id=$booking_id;
        $booking_item->booked_item_id=$request->input('booked_item_id');
        $booking_item->booked_item_amount=$request->input('booked_item_amount');
        $booking_item->booking_date=$request->input('booking_date');
        $booking_item->booking_guests=$request->input('booking_guests');
        $booking_item->save();
        $booking_user=new Booked_user;
        $booking_user->booking_id=$booking_id;
        $booking_user->booked_user_name=$request->input('booked_user_name');
        $booking_user->booked_user_email=$request->input('booked_user_email');
        $booking_user->booked_user_phone=$request->input('booked_user_phone');
        $booking_user->booked_user_company=$request->input('booked_user_company');
        $booking_user->booked_user_message=$request->input('booked_user_message');
        $booking_user->booking_guests=$request->input('booking_guests');
        $booking_user->save();
        $booking_log=new Booking_log;
        $booking_log->booking_id=$booking_id;
        $booking_log->log_time=$request->input('booking_time');
        $booking_log->log_date=$request->input('booking_date');
        $booking_log->save();
        

        return view('guest.success')->with(['response'=> $booking_id] ); 
    }
    public function check_booking(Request $request)
    {
        $org_id=$request->input('organization_id');
        $date=$request->input('booking_date2');
        $time=$request->input('booking_time2');
        $guest=$request->input('booking_guests2');


        $booking_log=DB::table('booking_logs')
        ->join('bookings','booking_logs.booking_id','=','bookings.id')
        ->select('booking_logs.*','bookings.*')
        ->where(['booking_logs.log_date'=>$date,'bookings.organization_id'=>$org_id])
        ->count();


        // $booking_log=Booking_log::where('log_date',$request->input('booking_date2'))
        // ->count();
        return redirect('/guest/organizationview/'.$org_id)->with([
            'booking_log'=> 1,
            'result'=> $booking_log,'booking_date2'=> $date,
            'booking_time2'=> $time,'booking_guests2'=> $guest] );
    }
    public function getreservation($id)
    {
        
        $booking_log=Booking_log::where('id',$id)
        ->count();
        return response($booking_log)->with(['response'=>$booking_log] );
    }
}

