<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        
    }
    public function add()
    {
        return view('dashboard.addcategory');
    }
    public function added(Request $request)
    {
        $this->validate($request,
            [
                'category_title'=> 'required',
            ]
            );
        $category=new Category;
        $category->category_title=$request->input('category_title');
        $category->category_info=$request->input('category_info');
        $category->category_icon=$request->input('category_icon');
        $category->category_type=$request->input('category_type');
        $category->category_color=$request->input('category_color');
        $category->save();
        return redirect('dashboard/category/add')->with(['response'=> $category->category_title . '  Added Successfully'] ); 
    }
    public function list()
    {
        $category=Category::get();
        return view('dashboard.categorylist',['categories'=>$category]); 
    }
    public function enable($id)
    {
        $category=Category::get();
        $category_update=new Category;
        $category_update->status=1;
        $data=array(
            'status' => 1 
                );

        Category::where('id',$id)->update($data);
        $category_update->update();
        return redirect('/dashboard/category/list')->with(['categories'=>$category,'response'=>'Category is Enabled'] ); ; 
    }
    public function disable($id)
    {
        $category=Category::get();
        $category_update=new Category;
        $category_update->status=0;
        $data=array(
            'status' => 0 
                );

        Category::where('id',$id)->update($data);
        $category_update->update();
        return redirect('/dashboard/category/list')->with(['categories'=>$category,'response'=>'Category is Disabled'] ); ; 
    }
    public function delete($id)
    {
        $category=Category::get();        
        Category::where('id',$id)->delete();

        return redirect('/dashboard/category/list')->with(['categories'=>$category,'response'=>'Category Has Deleted Successfully'] ); ; 
    }
}
