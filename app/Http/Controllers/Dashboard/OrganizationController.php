<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Organization;
class OrganizationController extends Controller
{
    public function list()
    {
        $organization=Organization::get();
        return view('dashboard.organizationlist',['organizations'=>$organization]); 
    }
    public function enable($id)
    {
        $category=Organization::get();
        $category_update=new Organization;
        $category_update->status=1;
        $data=array(
            'status' => 1 
                );

                Organization::where('id',$id)->update($data);
        $category_update->update();
        return redirect('/dashboard/organization/list')->with(['response'=>'Organization is Enabled'] ); ; 
    }
    public function disable($id)
    {
        $category=Organization::get();
        $category_update=new Organization;
        $category_update->status=0;
        $data=array(
            'status' => 0 
                );

            Organization::where('id',$id)->update($data);
        $category_update->update();
        return redirect('/dashboard/organization/list')->with(['response'=>'Organization is Disabled'] ); ; 
    }
}
