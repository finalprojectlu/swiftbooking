<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use App\Organization;
use App\Category;
use App\Schedule;
use App\Section_attribute_item;
use Auth;

class GuestController extends Controller
{
    
    public function category()
    {
        $category=Category:: where('status','=',1)
        ->get();
        return view('guest.category',['categories'=>$category]); 
    }
    public function catorganizationlist($id)
    {
        $organization=DB::table('organizations')
        ->join('users','organizations.user_id','=','users.id')
        ->select('organizations.*','users.*','organizations.id AS org_id')
        ->where('organizations.organization_category','=',$id)
        ->where('organizations.status','=',1)
        ->paginate(15);

        return view('guest.organizationlist',['organizations'=>$organization]); 
    }
    public function organizationview($id)
    {
        $organization=Organization::where('id','=',$id)
        ->where('status','=',1)
        ->first();

        $category=Category::find($organization->organization_category);

        $schedule=Schedule::where('organization_id','=',$id)
        ->where('status','=',1)
        ->first();

        $items =Section_attribute_item::where(['organization_id'=>$id])
        ->get();
        
        if($category->category_type==3)
            return view('guest.organizationviewrent',['organization'=>$organization,'items'=>$items,'schedule'=>$schedule,'category'=>$category]); 
        elseif($category->category_type==2)
            return view('guest.organizationviewseat',['organization'=>$organization,'items'=>$items,'schedule'=>$schedule,'category'=>$category]); 
        else
            return view('guest.organizationviewticket',['organization'=>$organization,'items'=>$items,'schedule'=>$schedule,'category'=>$category]);  
    }
}
