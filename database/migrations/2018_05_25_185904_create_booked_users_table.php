<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookedUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booked_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id');
            $table->string('booked_user_name');
            $table->string('booked_user_email')->nullable();;
            $table->string('booked_user_phone');
            $table->string('booked_user_company')->nullable();;
            $table->string('booked_user_message')->nullable();;
            $table->integer('booking_guests')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booked_users');
    }
}
