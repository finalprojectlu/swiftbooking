<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookedItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booked_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('booking_id');
            $table->integer('booked_item_id');
            $table->time('booked_item_amount')->nullable();
            $table->date('booking_date');
            $table->integer('booking_guests')->nullable();
            $table->integer('status')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booked_items');
    }
}
