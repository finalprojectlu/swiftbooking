<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('organization_name');
            $table->string('organization_graphical')->default(0)->nullable();
            $table->string('organization_cover')->nullable();
            $table->string('organization_link')->nullable();
            $table->integer('organization_category');
            $table->string('organization_info')->nullable();
            $table->string('organization_address')->nullable();
            $table->string('organization_map')->nullable();
            $table->string('organization_contact')->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
