<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectionAttributeItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_attribute_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('item_title');
            $table->integer('organization_id');
            $table->string('item_image1')->default(0);
            $table->string('item_image2')->default(0);
            $table->string('item_link')->nullable();
            $table->string('item_price')->nullable();
            $table->integer('item_price_type')->nullable();
            $table->string('item_info')->nullable();
            $table->string('item_features')->nullable();
            $table->string('item_status')->nullable();
            $table->string('item_rating')->nullable();
            $table->integer('item_discount')->nullable();
            $table->integer('section_attribute_id')->nullable();
            $table->integer('limit')->default(1)->nullable();
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_attribute_items');
    }
}
