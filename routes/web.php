<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/success', function () {
    return view('guest.success');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('/login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

// Admin Dashboard
Route::get('/dashboard', 'Dashboard\AdminController@index')->name('dashboard');
Route::get('/dashboard/category/list', 'Dashboard\CategoryController@list');
Route::get('/dashboard/category/add', 'Dashboard\CategoryController@add');
Route::post('/dashboard/category/added', 'Dashboard\CategoryController@added');
Route::get('/dashboard/category/delete/{id}', 'Dashboard\CategoryController@delete');
Route::get('/dashboard/category/enable/{id}', 'Dashboard\CategoryController@enable');
Route::get('/dashboard/category/disable/{id}', 'Dashboard\CategoryController@disable');
Route::get('/dashboard/organization/list', 'Dashboard\OrganizationController@list');
Route::get('/dashboard/organization/enable/{id}', 'Dashboard\OrganizationController@enable');
Route::get('/dashboard/organization/disable/{id}', 'Dashboard\OrganizationController@disable');


// User Dashboard
Route::get('/organization', 'User\OrganizationController@organizations');
Route::get('/organization/add', 'User\OrganizationController@addorganization');
Route::post('/organization/added', 'User\OrganizationController@addedorganization')->name('add_organization');
Route::post('/organization/finish', 'User\OrganizationController@finishorganization')->name('finishorganization');
Route::post('/organization/finishseat', 'User\OrganizationController@finishorganizationseat')->name('finishorganizationseat');
Route::post('/organization/update', 'User\OrganizationController@organizationupdate')->name('organizationupdate');
Route::post('/organization/addsection', 'User\OrganizationController@addsection')->name('addsection');
Route::get('/organization/view/{id}', 'User\OrganizationController@organizationview');
Route::get('/organization/edit/{id}', 'User\OrganizationController@organizationedit');
Route::get('/organization/item/delete/{id}', 'User\OrganizationController@itemdelete');
Route::get('/organization/item/edit/{id}', 'User\OrganizationController@itemedit');
Route::get('/organization/item/disable/{id}', 'User\OrganizationController@itemdisable');
Route::get('/organization/item/enable/{id}', 'User\OrganizationController@itemenable');
Route::get('/organization/confirmation', 'User\ConfirmationController@confirmation');
Route::get('/organization/confirmation/enable/{id}', 'User\ConfirmationController@enable');
Route::get('/organization/confirmation/disable/{id}', 'User\ConfirmationController@disable');


// guest user views
Route::get('/guest/category', 'GuestController@category');
Route::get('/guest/organizationlist/{id}', 'GuestController@catorganizationlist');
Route::get('/guest/organizationview/{id}', 'GuestController@organizationview');
Route::post('/guest/booked', 'BookingController@booking_store')->name('book_organization');
Route::post('/guest/check_booking', 'BookingController@check_booking')->name('check_booking');
// ajax
Route::get('/guest/getreservation/{$id}', 'GuestController@getreservation');
Route::post('/search', 'HomeController@search')->name('search');
