<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'facebook' => [

        'client_id' =>'164133097629633',         // Your facebook Client ID
        'client_secret' =>'93223bd609854555823db362fc018731', // Your facebook Client Secret
        'redirect' => 'http://localhost:8000/login/facebook/callback',
    ],
    'google' => [
        'client_id' => '1012884715409-mcroo4u953ts916j507qp4aa96nch4qo.apps.googleusercontent.com',         // Your facebook Client ID
        'client_secret' => 'THpfGm7vsfQNB4F_7dSCsuxX', // Your facebook Client Secret
        'redirect' => 'http://localhost:8000/login/google/callback',
    ],

];
