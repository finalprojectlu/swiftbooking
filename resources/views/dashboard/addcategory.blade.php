@extends('layouts.dashboard')

@section('dashboard')
<div class="container">
@include('layouts.alart')

    <div class="row justify-content-center">
        <div class="col-md-8">

            <!-- Card -->
            <div class="card">

                <!-- Card body -->
                <div class="card-body">

                    <!-- Material form register -->
                    <form  method="POST" class="col s12" action="{{ url('/dashboard/category/added') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}
                        <p class="h4 text-center py-4">Add Category</p>

                        <!-- Material input text -->
                        <div class="md-form">
                            <i class="fa fa-user prefix grey-text"></i>
                            <input type="text" id="category_title" name="category_title" class="form-control">
                            <label for="category_title" class="font-weight-light">Category Title</label>
                            @if ($errors->has('category_title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category_title') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!--// Material input text -->
                        <!-- Material input text -->
                        <div class="md-form">
                            <i class="fa fa-user prefix grey-text"></i>                           
                            <input type="text" id="category_icon" name="category_icon" class="form-control">
                            <label for="category_icon" class="font-weight-light">Category Icon(font awesome code)</label>
                            @if ($errors->has('category_icon'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category_icon') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!--// Material input text -->
                        <!-- Material input text -->
                        <div class="md-form">
                            <i class="fa fa-user prefix grey-text"></i>
                            <!--Blue select-->
                            <select  id="category_type" name="category_type" class=" mdb-select colorful-select dropdown-default ml-5 ">
                                <option value="1">Type 1:Ticket/Serial Booking</option>
                                <option value="2">Type 2: Graphical Seat Booking </option>
                                <option value="3">Type 3:Room/Vechile/Instrumet Rent</option>
                            </select>
                            <label  for="category_color" class="font-weight-light">Category Color</label>
                            <!--/Blue select--> 
                            <!-- <input type="text" id="category_color" name="category_color" class="form-control">
                            <label for="category_color" class="font-weight-light">Category Color</label> -->
                            @if ($errors->has('category_color'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category_color') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- Material input text -->
                        <div class="md-form">
                            <i class="fa fa-user prefix grey-text"></i>
                            <!--Blue select-->
                            <select  id="category_color" name="category_color" class=" mdb-select colorful-select dropdown-default ml-5">
                                <option value="7c87a5">7c87a5</option>
                                <option value="f39893">f39893</option>
                                <option value="f5a572">f5a572</option>
                                <option value="abd194">abd194</option>
                                <option value="7ac2da">7ac2da</option>
                                <option value="3079ac">3079ac</option>
                                <option value="f1c85e">f1c85e</option>
                                <option value="84a1c1">84a1c1</option>
                                <option value="21898c">21898c</option>
                                <option value="53bbb4">53bbb4</option>
                                <option value="b88cbf">b88cbf</option>
                                <option value="627a92">627a92</option>
                            </select>
                            <label  for="category_color" class="font-weight-light">Category Color</label>
                            <!--/Blue select--> 
                            <!-- <input type="text" id="category_color" name="category_color" class="form-control">
                            <label for="category_color" class="font-weight-light">Category Color</label> -->
                            @if ($errors->has('category_color'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category_color') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!--// Material input text -->
                        <!-- Material textarea message -->
                        <div class="md-form">
                            <i class="fa fa-pencil prefix grey-text"></i>
                            <textarea type="text" id="category_info" name="category_info" class="form-control md-textarea" rows="3"></textarea>
                            <label for="category_info">Category Info</label>
                            @if ($errors->has('category_info'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('category_info') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!--//Material textarea message -->

                        <div class="text-center py-4 mt-3">
                            <button class="btn btn-outline-default btn-rounded waves-effect btn-lg" type="submit">Add</button>
                        </div>
                    </form>
                    <!-- Material form register -->

                </div>
                <!-- Card body -->

            </div>
            <!-- Card -->
                      
                      
                  
                  
        </div>
    </div>
</div>
@endsection
