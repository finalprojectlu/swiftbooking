@extends('layouts.dashboard')

@section('dashboard')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- alert -->
    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <br/>
        <div class="col s12 m10 l6">
        <div class="card-panel red lighten-2"><span class="white-text">{{$errors}}</span></div>
        </div>
        @endforeach
    @endif
    @if(session('response'))
        <!--Panel-->
        <a onload="toastr.warning('Hi! I am warning message.');">
        <div class="row justify-content-center">
        <div class="col mb-3">
            <div class="card border-success ">
                <div class="card-body text-success text-center">
                    <h5 class="card-title">{{session('response')}}</h5>
                </div>
            </div>
        </div>
    </div>
            <!--/.Panel-->
    @endif

    <div class="row justify-content-center">
        <div class="col-md-10 mb-5">
           <!--Section: Basic examples-->
           <section>
           
                           <div class="row">
                               
                               <div class="col-md-12">
                                                           
                                   <div class="card">
                                       <div class="card-body">
                                           <table id="datatables" class="table table-striped table-bordered table-responsive-md" cellspacing="0" width="100%">
                                               <thead>
                                                   <tr>
                                                       <th>Title</th>
                                                       <th>Owner ID</th>
                                                       <th class="text-center">Status</th>
                                                   </tr>
                                               </thead>
                                               <tfoot>
                                                   <tr>
                                                   <th>Title</th>
                                                   <th>Owner ID</th>
                                                   <th class="text-center">Status</th>
                                               </tr>
                                               </tfoot>
                                               <tbody>
                                               @foreach($organizations->all() as $organization)
                                               <tr>
                                                    <td>{{ $organization->organization_name }}</td>
                                                    <td>{{ $organization->user_id }}</td>

                                                    
                                                    <td class="text-center">
                                                    @if( $organization->status == 0)
                                                    <a href="{{url('/dashboard/organization/enable/'.$organization->id)}}"  class="btn btn-success btn-rounded btn-sm">Enabale</a>
                                                    @elseif( $organization->status == 1)
                                                    <a href="{{url('/dashboard/organization/disable/'.$organization->id)}}" class="btn btn-danger btn-rounded btn-sm">Disable</a>

                                                    @endif
                                                    </td>
                                                </tr>
                                                
                                                @endforeach
                                               </tbody>
                                           </table>
                                       </div>
                                   </div>
           
                               </div>
           
                           </div>
           
                       </section>
                       <!--Section: Basic examples-->
                      
                  
                  
        </div>
    </div>
</div>


@endsection
