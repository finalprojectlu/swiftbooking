@extends('layouts.dashboard')

@section('dashboard')
<div class="container">
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <!-- alert -->
    @if(count($errors)>0)
        @foreach($errors->all() as $error)
        <br/>
        <div class="col s12 m10 l6">
        <div class="card-panel red lighten-2"><span class="white-text">{{$errors}}</span></div>
        </div>
        @endforeach
    @endif
    @if(session('response'))
        <!--Panel-->
        <a onload="toastr.warning('Hi! I am warning message.');">
        <div class="row justify-content-center">
        <div class="col mb-3">
            <div class="card border-success ">
                <div class="card-body text-success text-center">
                    <h5 class="card-title">{{session('response')}}</h5>
                </div>
            </div>
        </div>
    </div>
            <!--/.Panel-->
    @endif

    <div class="row justify-content-center">
        <div class="col-md-10 mb-5">
           <!--Section: Basic examples-->
           <section>
           
                           <div class="row">
                               
                               <div class="col-md-12">
                                                           
                                   <div class="card">
                                       <div class="card-body">
                                           <table id="datatables" class="table table-striped table-bordered table-responsive-md" cellspacing="0" width="100%">
                                               <thead>
                                                   <tr>
                                                       <th>Title</th>
                                                       <th>Info</th>
                                                       <th class="text-center">Action</th>
                                                       <th class="text-center">Status</th>
                                                   </tr>
                                               </thead>
                                               <tfoot>
                                                   <tr>
                                                   <th>Title</th>
                                                   <th>Info</th>
                                                   <th class="text-center">Action</th>
                                                   <th class="text-center">Status</th>
                                               </tr>
                                               </tfoot>
                                               <tbody>
                                               @foreach($categories->all() as $itemcategory)
                                               <tr>
                                                    <td>{{ $itemcategory->category_title }}</td>
                                                    <td>{{ $itemcategory->category_info }}</td>
                                                    <td class="text-center">
                                                        <a href="{{url('/dashboard/category/edit/'.$itemcategory->id)}}"><i class="fa fa-edit fa-lg blue-text" aria-hidden="true"></i></a>                                                
                                                        <a data-toggle="modal" data-target="#deleteModal"><i class="fa fa-remove fa-lg red-text" aria-hidden="true"></i></a>
                                                    </td>
                                                    
                                                    <td class="text-center">
                                                    @if( $itemcategory->status == 0)
                                                    <a href="{{url('/dashboard/category/enable/'.$itemcategory->id)}}"  class="btn btn-success btn-rounded btn-sm">Enabale</a>
                                                    @elseif( $itemcategory->status == 1)
                                                    <a href="{{url('/dashboard/category/disable/'.$itemcategory->id)}}" class="btn btn-danger btn-rounded btn-sm">Disable</a>

                                                    @endif
                                                    </td>
                                                </tr>
                                                <!-- Modal -->
                                                <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Delete {{ $itemcategory->category_title }}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                Are you sure that you want to delete {{ $itemcategory->category_title }} permanently, this will not be undo after confermation
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                <a href="{{url('/dashboard/category/delete/'.$itemcategory->id)}}" class="btn btn-success">Yes, Delete</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Modal Ends-->
                                                @endforeach
                                               </tbody>
                                           </table>
                                       </div>
                                   </div>
           
                               </div>
           
                           </div>
           
                       </section>
                       <!--Section: Basic examples-->
                      
                  
                  
        </div>
    </div>
</div>


@endsection
