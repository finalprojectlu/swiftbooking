<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
        <!-- font -->
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Righteous" rel="stylesheet">

    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}">
    <!-- Bootstrap core CSS -->
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('css/compiled.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<!--Nav-->
<div class="container-fluid header blue-gradient">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2"> 
                    <a href="{{ url('/') }}">
                        SwiftBooking
                    </a>
                    
                </div>
                <div class="col-md-10 col-sm-10 text-right">
                     @guest
                        <a  href="{{ route('login') }}">Login</a>
                        <a  href="{{ route('register') }}">Register</a>
                    @else
                    <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        
                    @endguest
                    
                </div>
            </div>
        </div>  
        <!--Search bar-->
    <div class="container search">
        <h4 class="text-center text-white">All Categories</h4>
        <br>
        <div class="row">
            <div class="offset-md-4 col-md-4">
            <form method="POST" action="{{ route('search') }}"enctype="multipart/form-data">
                @csrf
                <div class="input-group">
                    <input class="form-control" name="search" type="text" placeholder="Search..." aria-label="Search" style=" border-radius: 40px;" id="mysearch">
                    <div class="input-group-addon" style="margin-left: -50px; z-index: 3; border-radius: 40px; background-color: transparent; border:none;">
                        <button class="btn btn-primary btn-md" type="submit" style="border-radius: 20px;" id="search-btn"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>    
    @yield('content')

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
</body>
</html>
