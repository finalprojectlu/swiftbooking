<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Righteous" rel="stylesheet">

    <title>SwiftBooking</title>

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('css/compiled.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <!-- DataTables.net --> 
    <link href="{{ asset('js/vendor/datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <!-- <style>
    html,
    body,
    header,
    .jarallax {
      height: 700px;
    }

    @media (max-width: 740px) {
      html,
      body,
      header,
      .jarallax {
        height: 100vh;
      }
    }

     @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .jarallax {
        height: 100vh;
      }
    }

    @media (min-width: 560px) and (max-width: 660px) {
      header .jarallax h5 {
        display: none !important;
      }
    }

    .page-footer {
      margin-top: 0px;
      padding-top: 0px;
    }

    .avatar {
      max-width: 170px;
    }
  </style> -->

<style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><style type="text/css" id="#jarallax-clip-0">#jarallax-container-0 {
   clip: rect(0 1903px 700px 0);
   clip: rect(0, 1903px, 700px, 0);
}</style>
</head>
<body class="fixed-sn light-blue-skin">


  <!--Main Navigation-->
  <header>
	<?php
								$user=Auth::user();
								$requestCount=DB::table('bookings')
								->where(['owner_id'=>$user->id,'status'=>1])
								->count();
								?>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar blue-gradient">
      <div class="container">
      <a class="navbar-brand" href="/organization">
				<strong>SwiftBooking</strong>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent-7">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
					
					</li>

				</ul>
				<form class="form-inline">
					<div class="md-form mt-0">
					
					<!-- Right Side Of Navbar -->
					<ul class="nav navbar-nav nav-flex-icons">
  
                <li class="nav-item">
                    <a href="{{ url('organization/confirmation') }}"class="nav-link btn btn-sm"><i class="fa fa-comments-o"></i> <span class="clearfix d-none d-sm-inline-block"> Booking Request <span class="badge blue p-1">{{$requestCount}}</span></span></a>
                </li>
                <li class="nav-item">
										
										<a class="nav-link btn btn-sm pl-3 pr-3" href="{{ route('logout') }}"
												onclick="event.preventDefault();
																			document.getElementById('logout-form').submit();">
																			<i class="fa fa-user"></i>  Logout
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												@csrf
										</form>
									</a>
                </li>
                
            </ul>

					</div>
				</form>
				</div>
      </div>
    </nav>

  </header>
  <!--Main Navigation-->


    <!--Main Layout-->
    <main >
    <div class="pt-3" id="app">
            @yield('organization')
    </div>
    </main>
 <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
    <!-- DataTables.net -->
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <!-- timepicker -->
    <script>
      // Time Picker Initialization
      $('#starting_time').pickatime({
        autoclose: true
      });
      // Time Picker Initialization
      $('#ending_time').pickatime({
        autoclose: true
      });
    </script>
    <script>
    //options select
        $(document).ready(function() {
            $('.mdb-select').material_select();
        });
        
        // SideNav Button Initialization
        $(".button-collapse").sideNav();
        
                var container = document.querySelector('.custom-scrollbar');
                Ps.initialize(container, {
                    wheelSpeed: 2,
                    wheelPropagation: true,
                    minScrollbarLength: 20
                });
        
                $(document).ready(function() {
                    $('#datatables').DataTable();
                });
        
                // Material Select Initialization
                $(document).ready(function () {
                    $('select[name="datatables_length"]').material_select();
                });
    </script>
    <script>
    new WOW().init();

    // MDB Lightbox Init
    $(function () {
      $("#mdb-lightbox-ui").load("../../mdb-addons/mdb-lightbox-ui.html");
    });
    
  </script>
    <script>
    // Tooltips Initialization
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// Steppers                
$(document).ready(function () {
  var navListItems = $('div.setup-panel-2 div a'),
          allWells = $('.setup-content-2'),
          allNextBtn = $('.nextBtn-2'),
          allPrevBtn = $('.prevBtn-2');

  allWells.hide();

  navListItems.click(function (e) {
      e.preventDefault();
      var $target = $($(this).attr('href')),
              $item = $(this);

      if (!$item.hasClass('disabled')) {
          navListItems.removeClass('btn-amber').addClass('btn-blue-grey');
          $item.addClass('btn-amber');
          allWells.hide();
          $target.show();
          $target.find('input:eq(0)').focus();
      }
  });
  
  allPrevBtn.click(function(){
      var curStep = $(this).closest(".setup-content-2"),
          curStepBtn = curStep.attr("id"),
          prevStepSteps = $('div.setup-panel-2 div a[href="#' + curStepBtn + '"]').parent().prev().children("a");

          prevStepSteps.removeAttr('disabled').trigger('click');
  });

  allNextBtn.click(function(){
      var curStep = $(this).closest(".setup-content-2"),
          curStepBtn = curStep.attr("id"),
          nextStepSteps = $('div.setup-panel-2 div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
          curInputs = curStep.find("input[type='text'],input[type='url']"),
          isValid = true;

      $(".form-group").removeClass("has-error");
      for(var i=0; i< curInputs.length; i++){
          if (!curInputs[i].validity.valid){
              isValid = false;
              $(curInputs[i]).closest(".form-group").addClass("has-error");
          }
      }

      if (isValid)
          nextStepSteps.removeAttr('disabled').trigger('click');
  });

  $('div.setup-panel-2 div a.btn-amber').trigger('click');
});

    </script>

</body>
<!--/.Double navigation-->

        

   
</html>

<script>
	$(document).ready(function(){
		var i=1;
		$('#add').click(function(){
		i++;

		$('#dynamic_sector').before($('#forappend').html());
		});
		

		
		// $('#submit').click(function(){		
		// 	$.ajax({
		// 		url:"name.php",
		// 		method:"POST",
		// 		data:$('#add_name').serialize(),
		// 		success:function(data)
		// 		{
		// 			alert(data);
		// 			$('#add_name')[0].reset();
		// 		}
		// 	});
		// });
		
	});
</script>