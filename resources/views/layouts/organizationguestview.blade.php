<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'SwiftBooking') }}</title>

    <!-- Styles -->
        <!-- font -->
        <link href="https://fonts.googleapis.com/css?family=Comfortaa|Righteous" rel="stylesheet">

    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}">
    <!-- Bootstrap core CSS -->
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('css/compiled.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">


<style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><style type="text/css" id="#jarallax-clip-0">#jarallax-container-0 {
   clip: rect(0 1903px 700px 0);
   clip: rect(0, 1903px, 700px, 0);
}</style>
</head>
<body class="fixed-sn light-blue-skin">

		<!--Navbar-->
		<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar blue-gradient">
        <div class="container">
            <a class="navbar-brand" href="#"><strong>SwiftBooking</strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{url('guest/category')}}">Categories<span class="sr-only"></span></a>
                    </li>
                    
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                @guest
                        <a class="nav-link btn btn-sm pl-3 pr-3"  href="{{ route('login') }}">Login</a>
                        <a class="nav-link btn btn-sm pl-3 pr-3"  href="{{ route('register') }}">Register</a>
                    @else
                        <!-- Right Side Of Navbar -->
						<?php
								$user=Auth::user();
								$requestCount=DB::table('bookings')
								->where(['owner_id'=>$user->id,'status'=>1])
								->count();
								?>
                        <li class="nav-item">
                            <a href="{{ url('organization/confirmation') }}"class="nav-link btn btn-sm"><i class="fa fa-comments-o"></i> <span class="clearfix d-none d-sm-inline-block"> Booking Request <span class="badge blue p-1">{{$requestCount}}</span></span></a>
                        </li>
                        <li class="nav-item">
                          
                        <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                        </li>

                    @endguest
            </ul>
            </div>
        </div>
		</nav>

	

    @yield('content')

    <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>

</body>
</html>
@yield('script')
