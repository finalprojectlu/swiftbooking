@if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
@endif
    <!-- alert -->

@if(count($errors)>0)
    @foreach($errors->all() as $error)
    <br/>
    <div class="row ">
        <div class="col-md-12">
            <div class="card border-danger ">
                <div class="card-body text-danger text-center">
                    <h5 class="card-title">Please Fill All Inputs Properly</h5>
                </div>
            </div>
        </div>
    </div>
    @endforeach
@endif
@if(session('response'))
    <!--Panel-->
    <div class="row ">
        <div class="col-md-12">
            <div class="card border-success ">
                <div class="card-body text-success text-center">
                    <h5 class="card-title">{{session('response')}}</h5>
                </div>
            </div>
        </div>
    </div>
    <!--/.Panel-->
@endif