<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SwiftBooking</title>
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Righteous" rel="stylesheet">
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('css/compiled.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <!-- DataTables.net --> 
    <link href="{{ asset('js/vendor/datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>
    html,
    body,
    header,
    .jarallax {
      height: 700px;
    }

    @media (max-width: 740px) {
      html,
      body,
      header,
      .jarallax {
        height: 100vh;
      }
    }

     @media (min-width: 800px) and (max-width: 850px) {
      html,
      body,
      header,
      .jarallax {
        height: 100vh;
      }
    }

    @media (min-width: 560px) and (max-width: 660px) {
      header .jarallax h5 {
        display: none !important;
      }
    }

    .page-footer {
      margin-top: 0px;
      padding-top: 0px;
    }

    .avatar {
      max-width: 170px;
    }
  </style>

<style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><style type="text/css" id="#jarallax-clip-0">#jarallax-container-0 {
   clip: rect(0 1903px 700px 0);
   clip: rect(0, 1903px, 700px, 0);
}</style>
</head>
<body class="fixed-sn light-blue-skin">


	<!--Main Navigation-->
	<header>
	<?php
								$user=Auth::user();
								$requestCount=DB::table('bookings')
								->where(['owner_id'=>$user->id,'status'=>1])
								->count();
								?>
		<!--Navbar-->
		<nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
			<div class="container">
				<a class="navbar-brand" href="/organization">
				<strong>SwiftBooking</strong>
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent-7">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
					
					</li>

				</ul>
				<form class="form-inline">
					<div class="md-form mt-0">
					
					<!-- Right Side Of Navbar -->
					<ul class="nav navbar-nav nav-flex-icons">
  
                <li class="nav-item">
                    <a href="{{ url('organization/confirmation') }}"class="nav-link btn btn-sm"><i class="fa fa-comments-o"></i> <span class="clearfix d-none d-sm-inline-block"> Booking Request <span class="badge blue p-1">{{$requestCount}}</span></span></a>
                </li>
                <li class="nav-item">
				<a class="nav-link btn btn-sm pl-3 pr-3" href="{{ route('logout') }}"
				onclick="event.preventDefault();
				document.getElementById('logout-form').submit();">
				<i class="fa fa-user"></i>  Logout
										</a>

										<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												@csrf
										</form>
									</a>
                </li>
                
            </ul>

					</div>
				</form>
				</div>
			</div>
		</nav>

		<!-- Intro Section -->
		<div class="view jarallax" data-jarallax="{&quot;speed&quot;: 0.2}" style="background-image: none; background-repeat: no-repeat; background-size: auto; background-position: center center; z-index: 0; background-attachment: scroll;" data-jarallax-original-styles="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient3.png); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="mask rgba-indigo-slight">
				<div class="container h-100 d-flex justify-content-center align-items-center">
					<div class="row pt-5 mt-3">
						<div class="col-md-12 mb-3">
							<div class="intro-info-content text-center">
								<h1 class="display-3 blue-text mb-5 wow fadeInDown" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInDown; animation-delay: 0.3s;">Swift<a class="blue-text font-weight-bold">Booking</a>
								</h1>
								<h5 class="text-uppercase blue-text mb-5 mt-1 font-weight-bold wow fadeInDown" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInDown; animation-delay: 0.3s;">your all reservation management system</h5>
								<div class="wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-name: fadeInUp; animation-delay: 0.4s;">
									<a href="{{ url('organization/add') }}" class="btn btn-blue btn-lg  btn-rounded waves-effect"><i class="fa fa-plus left"></i> Start New Organization</a>
								</div>
								<?php
								$user=Auth::user();
								$requestCount=DB::table('bookings')
								->where(['owner_id'=>$user->id,'status'=>1])
								->count();
								?>
								<div class="wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-name: fadeInUp; animation-delay: 0.4s;">
									<a href="{{ url('organization/confirmation') }}" class="btn btn-teal btn-rounded waves-effect"><i class="fa fa-info left"></i> Booking Request <span class="badge red">{{$requestCount}}</span></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="jarallax-container-0" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -100;">
				<div style="background-position: 50% 50%; background-size: 100%; background-repeat: no-repeat; background-image: url(&quot;https://mdbootstrap.com/img/Photos/Others/gradient3.png&quot;); position: fixed; top: 0px; left: 0px; width: 1903px; height: 1268.39px; overflow: hidden; pointer-events: none; margin-left: 0px; margin-top: -171.695px; visibility: visible; transform: translateY(-22.5px) translateZ(0px);">
				</div>
			</div>
		</div>

  	</header>
  	<!--Main Navigation-->


    <!--Main Layout-->
    <main >
		@yield('user')
    </main>
 	<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
    <!-- DataTables.net -->
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    //options select
        $(document).ready(function() {
            $('.mdb-select').material_select();
        });
        
        // SideNav Button Initialization
        $(".button-collapse").sideNav();
        
			var container = document.querySelector('.custom-scrollbar');
			Ps.initialize(container, {
				wheelSpeed: 2,
				wheelPropagation: true,
				minScrollbarLength: 20
			});
	
			$(document).ready(function() {
				$('#datatables').DataTable();
			});
	
			// Material Select Initialization
			$(document).ready(function () {
				$('select[name="datatables_length"]').material_select();
			});
    </script>
	<script>
		new WOW().init();

		// MDB Lightbox Init
		$(function () {
		$("#mdb-lightbox-ui").load("../../mdb-addons/mdb-lightbox-ui.html");
		});
  	</script>
</body>
<!--/.Double navigation-->

        

   
</html>
