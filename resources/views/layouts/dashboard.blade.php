<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SwiftBooking</title>
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Righteous" rel="stylesheet">

    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css') }}">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('css/compiled.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <!-- DataTables.net --> 
    <link href="{{ asset('js/vendor/datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body class="fixed-sn light-blue-skin">

    <!--Double navigation-->
    <header>
        <!-- Sidebar navigation -->
        <div id="slide-out" class="side-nav sn-bg-4 fixed mdb-sidenav">
            <ul class="custom-scrollbar list-unstyled" style="max-height:100vh;">
                <!-- Logo -->
                <li>
                    
                </li>
                <!--/. Logo -->
                <!--Social-->
                <!-- <li>
                    <ul class="social">
                        <li><a href="#" class="icons-sm fb-ic"><i class="fa fa-facebook"> </i></a></li>
                        <li><a href="#" class="icons-sm pin-ic"><i class="fa fa-pinterest"> </i></a></li>
                        <li><a href="#" class="icons-sm gplus-ic"><i class="fa fa-google-plus"> </i></a></li>
                        <li><a href="#" class="icons-sm tw-ic"><i class="fa fa-twitter"> </i></a></li>
                    </ul>
                </li> -->
                <!--/Social-->
                <!--Search Form-->
                <li>
                <form method="POST" class="search-form"action="{{ route('search') }}"enctype="multipart/form-data">
                @csrf
                                <div class="form-group md-form mt-0 pt-1 waves-light">
                                    <input type="text"name="search" class="form-control" placeholder="Search">
                                </div>
                            </form>
                </li>
                <!--/.Search Form-->
                <!-- Side navigation links -->
                <li>
                    <ul class="collapsible collapsible-accordion">
                        <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-chevron-right"></i> Categorries<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                    <li><a href="{{ url('dashboard/category/add')}}" class="waves-effect">Add Category</a>
                                    </li>
                                    <li><a href="{{ url('dashboard/category/list')}}" class="waves-effect">Category List</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        <li><a class="collapsible-header waves-effect arrow-r"><i class="fa fa-eye"></i> Organizations<i class="fa fa-angle-down rotate-icon"></i></a>
                            <div class="collapsible-body">
                                <ul>
                                <li><a href="{{ url('dashboard/organization/list')}}" class="waves-effect">Organizations List</a>
                                    </li>
                                    <li><a href="#" class="waves-effect">Banned Organization</a>
                                    </li>
                                    <li><a href="#" class="waves-effect">Banned Organization</a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                    </ul>
                </li>
                <!--/. Side navigation links -->
            </ul>
            <div class="sidenav-bg mask-strong"></div>
        </div>
        <!--/. Sidebar navigation -->
        <!-- Navbar -->
        <nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav">
            <!-- SideNav slide-out button -->
            <div class="float-left">
                <a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a>
            </div>
            <!-- Breadcrumb-->
            <div class="breadcrumb-dn mr-auto">
                <p>Dashboard || Admin</p>
            </div>
            <ul class="nav navbar-nav nav-flex-icons ml-auto">
                
            </ul>
        </nav>
        <!-- /.Navbar -->
    </header>
    <!--/.Double navigation-->
    <!--Main Layout-->
    <main >
            @yield('dashboard')
    </main>
 <!-- Scripts -->
    <!-- <script src="{{ asset('js/app.js') }}"></script> -->
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
    <!-- DataTables.net -->
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    //options select
        $(document).ready(function() {
            $('.mdb-select').material_select();
        });
        
        // SideNav Button Initialization
        $(".button-collapse").sideNav();
        
                var container = document.querySelector('.custom-scrollbar');
                Ps.initialize(container, {
                    wheelSpeed: 2,
                    wheelPropagation: true,
                    minScrollbarLength: 20
                });
        
                $(document).ready(function() {
                    $('#datatables').DataTable();
                });
        
                // Material Select Initialization
                $(document).ready(function () {
                    $('select[name="datatables_length"]').material_select();
                });
    </script>
</body>
<!--/.Double navigation-->

        

   
</html>
