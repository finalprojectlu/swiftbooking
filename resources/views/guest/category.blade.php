@extends('layouts.guest')
@section('content')

@include('layouts.alart')


<!--Categories-->
<div class="container categories">
    <div class="row">
        @foreach($categories->all() as $category)
        <!--Category Item -->
        <div class="col-md-3 mb-3">
            <a href=" {{url("/guest/organizationlist/{$category->id}") }}">
            <div class="categorydiv" style="background-color: #{{$category->category_color}};">
                <div class="img_div text-center"> 
                <i class="{{$category->category_icon}} fa-4x black-text"></i> 
                </div>    
                <button class="fixed-bottom btn btn-block btn-grey" >{{$category->category_title}}</button>
            </div>
            </a>
        </div>
        <!-- category item end -->
        @endforeach
    </div>

</div>


@endsection
