@extends('layouts.guest')
@section('content')

@include('layouts.alart')


<!--Categories-->
<div class="container categories">
    <div class="row text-center">

            

            <!--Grid column-->
            <div class="col-md-4 offset-md-4 mb-md-0 mb-5">

              <div class="testimonial">
                <!--Avatar-->
                <div class="avatar mx-auto">
                  <img src="{{asset('img/success.jpg')}}" class=" img-fluid">
                </div>
                <!--Content-->
                <h4 class="font-weight-bold green-text mt-4">Booked Successfully</h4>
                <h6 class="font-weight-bold blue-text my-3">
                @if(isset($responce))
                Booked ID #{{$responce}}
                @endif

                </h6>
                
              </div>

            </div>
            <!--Grid column-->

            

          </div>


</div>


@endsection
