@extends('layouts.guest')
@section('content')




<!--Categories-->
<div class="container categories">
@if(isset($response))
        <!--Panel-->
        <div class="row ">
        <div class="col-md-12">
            <div class="card border-success mb-2">
                <div class="card-body text-success text-center">
                    <h5 class="card-title">{{$response}}</h5>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row">

        @foreach($organizations->all() as $organization)
        <!--organization Item -->
        <div class="card card-body fixedcard-200">
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-4 col-xl-4">

      <!-- Featured image -->
      <div class="view overlay rounded z-depth-1-half mb-lg-0 mb-4">
        @if($organization->organization_cover)
        <img class="img-fluid" src="{{ $organization->organization_cover}}" alt="organization">
        @else
        <img class="img-fluid" src="{{ asset('img/organization.jpg') }}" alt="cover">
        @endif        
        <a>
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-8 col-xl-8">

      <!-- Post title -->
      <h3 class="font-weight-bold mb-3"><strong>{{$organization->organization_name}}</strong></h3>
      <!-- Excerpt -->
      <p class="dark-grey-text">{{$organization->organization_info}}</p>
      <!-- Post data -->
      <p>by <a class="font-weight-bold">{{$organization->name}}</a>, {{date('M j, Y H:i', strtotime($organization->updated_at))}}</p>
      <!-- Read more button -->
      <div class="">
      <a  class="btn btn-outline-primary btn-rounded" href="{{url("/guest/organizationview/{$organization->org_id}")}}" >Book Now</a>
    </div>
    </div>
    <!-- Grid column -->

  </div>
  
  </div>
        @endforeach
        <div class="col-12 text-center mt-3">
        {{$organizations->links()}}
</div>
    </div>

</div>


@endsection
