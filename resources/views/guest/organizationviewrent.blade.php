@extends('layouts.organizationguestview')
@section('content')

	<!-- Intro Section -->
    @if($organization->organization_cover)
        <div class="view jarallax" data-jarallax="{&quot;speed&quot;: 0.2}" style="background-image: none; background-repeat: no-repeat; background-size: auto; background-position: center center; z-index: 0; background-attachment: scroll;" data-jarallax-original-styles="background-image: url({{$organization->organization_cover}}); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    @else
        <div class="view jarallax" data-jarallax="{&quot;speed&quot;: 0.2}" style="background-image: none; background-repeat: no-repeat; background-size: auto; background-position: center center; z-index: 0; background-attachment: scroll;" data-jarallax-original-styles="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient3.png); background-repeat: no-repeat; background-size: cover; background-position: center center;">
    @endif
        <div class="view jarallax" data-jarallax="{&quot;speed&quot;: 0.2}" style="background-image: none; background-repeat: no-repeat; background-size: auto; background-position: center center; z-index: 0; background-attachment: scroll;" data-jarallax-original-styles="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient3.png); background-repeat: no-repeat; background-size: cover; background-position: center center;">
			<div class="mask rgba-white-strong">
				<div class="container h-100 d-flex justify-content-center align-items-center">
					<div class="row pt-5 mt-3">
						<div class="col-md-12 mb-3">
							<div class="intro-info-content text-center">
                                <div class="foo">
                                    <span class="letter" data-letter="{{$organization->organization_name }}">{{$organization->organization_name }}</span>
                                </div>
                                <h5 class="text-uppercase blue-text mb-5 mt-1 font-weight-bold wow fadeInDown" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeInDown; animation-delay: 0.3s;">{{$category->category_title }}</h5>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="jarallax-container-0" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -100;">
			@if($organization->organization_cover)
                <div style="background-position: 50% 50%; background-size: 100%; background-repeat: no-repeat; background-image: url(&quot;{{$organization->organization_cover}}&quot;); position: fixed; top: 0px; left: 0px; width: 1903px; height: 1268.39px; overflow: hidden; pointer-events: none; margin-left: 0px; margin-top: -171.695px; visibility: visible; transform: translateY(-22.5px) translateZ(0px);">
            @else
            	<div style="background-position: 50% 50%; background-size: 100%; background-repeat: no-repeat; background-image: url(&quot;https://mdbootstrap.com/img/Photos/Others/gradient3.png&quot;); position: fixed; top: 0px; left: 0px; width: 1903px; height: 1268.39px; overflow: hidden; pointer-events: none; margin-left: 0px; margin-top: -171.695px; visibility: visible; transform: translateY(-22.5px) translateZ(0px);">
            @endif
                </div>
			</div>
		</div>
        @include('layouts.alart')
        <!--    Reservation-->
    <div class="container-fluid reservation ">

    <h2 class="text-center">Reservation</h2>
    
    <br>
    <br>
    <!-- <form id="Form2" method="POST"  name="Form2" action="{{ route('check_booking') }}">
    @csrf -->
    <?php $x=0 ?>
    <h4 class="text-center">Make your reservation</h4>
    @if(session('booking_log'))
    @if($schedule->booking_limit-session('result')>=session('booking_guests2'))
    <?php $x=1 ?>
    <div class="row ">
        <div class="col-md-12">
            <div class="card border-success ">
                <div class="card-body text-success text-center">
                    <h5 class="card-title">Yes, {{session('booking_guests2')}} Tickets Are Avalaible</h5>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="row ">
        <div class="col-md-12">
            <div class="card text-warning ">
                <div class="card-body text-warning text-center">
                    <h5 class="card-title">Not Avalaible, Please Select Different Schedule</h5>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endif
    <div class="container search_bar z-depth-1">
    <form  method="POST" action="{{ route('check_booking') }}"  name="uploadForm"  enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="organization_id" value="{{$organization->id}}">
    <input type="hidden" name="owner_id"  value="{{$organization->user_id}}">
        @if(session('booking_log'))
        
            <div class="row">
            
                <div class="col-md-3 time">
                    <label class="color" for="name">
                        Time
                    </label>

                    <br>
                    <input  type="time" name="booking_time2" id="booking_time2"required="required"  value="{{session('booking_time2')}}"class="form-control validate" width="276" require/>
                    @if ($errors->has('booking_time2'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('booking_time2') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3 date">
                    <label class="color" for="name">
                        Date
                    </label>
                
                    <br>
                    <input name="booking_date2"  id="booking_date2" value="{{session('booking_date2')}}" type="date" required="required" class="form-control validate">
                    @if ($errors->has('booking_date2'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('booking_date2') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3 guest">
                <label for="exampleForm2" class="color">Guest</label>
                <input type="number"name="booking_guests2"id="booking_guests2"   @if($x==1) value="{{session('booking_guests2')}}"  @endif id="booking_guests2" required="required" class="form-control validate" style="border-bottom: 1px grey solid;" required>
                    @if ($errors->has('booking_guests2'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('booking_guests2') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3 check">
                    <button id="readdata" type="submit" class="btn blue-gradient mx-auto d-block">CHECK AVAILABILITY</button>
                </div>
            </div>

        @else
                    
            <div class="row">
                <div class="col-md-3 time">
                    <label class="color" for="name">
                        Time
                    </label>
                    <br>
                    <input  type="time" name="booking_time2" id="booking_time2"value="{{ old('booking_time2') }}" required="required" class="form-control validate" width="276" require/>
                    @if ($errors->has('booking_time'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('booking_time2') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3 date">
                    <label class="color" for="name">
                        Date
                    </label>
                
                    <br>
                    <input name="booking_date2"  id="booking_date2" value="{{ old('booking_date2') }}" type="date" required="required" class="form-control validate">
                    @if ($errors->has('booking_date2'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('booking_date2') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3 guest">
                <label for="exampleForm2" class="color">Guest</label>
                <input type="number"name="booking_guests2"id="booking_guests2" value="{{ old('booking_guests2') }}"  id="booking_guests2" required="required" class="form-control validate" style="border-bottom: 1px grey solid;">
                    @if ($errors->has('booking_guests2'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('booking_guests2') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-md-3 check">
                    <button id="readdata" type="submit" class="btn blue-gradient mx-auto d-block">CHECK AVAILABILITY</button>
                </div>

            </div>
                    <!-- </form>    -->
                    @endif
                    </form>
            </div>

    <!--       seat-->
    <form  method="POST" action="{{ route('book_organization') }}" id="uploadForm" name="uploadForm"  enctype="multipart/form-data">
    @csrf
    @if(session('booking_log'))
    <input type="hidden" name="booking_date"  id="booking_date" value="{{session('booking_date2')}}" type="date"required="required" class="form-control validate">
    <input type="hidden"name="booking_guests"  id="booking_guests"  value="{{session('booking_guests2')}}"  required="required" class="form-control validate" style="border-bottom: 1px grey solid;">  
    <input  type="hidden" name="booking_time"id="booking_time" @if($x==1)value="{{session('booking_time2')}}" @endif required="required" class="form-control validate" width="276" />
    @endif

<!--Org Info-->
            <h4 class="text-center">Confirm your reservation</h4>
                <div class="container ">
                    <!-- Section: Contact v.3 -->
        <section class="contact-section my-5">

        <!-- Form with header -->
        <div class="card">

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-lg-6">
            <div class="card-body   ">

                <!-- Default checkbox -->
                <div class="ml-5">
                    <input class="form-check-input" type="checkbox" name="sunday" value="1"  id="sunday"
                    @if($schedule->sunday)checked="checked"@endif disabled
                    >
                    <label class="form-check-label" for="sunday">
                        Sunday
                    </label>
                </div>
                <div class="ml-5">
                    <input class="form-check-input" type="checkbox"  name="monday" value="2" id="monday"
                    @if($schedule->monday)checked="checked"@endif disabled
>
                    <label class="form-check-label" for="monday">
                        Monday
                    </label>
                </div>
                <div class="ml-5">
                    <input class="form-check-input" type="checkbox"  name="tuesday" value="3" id="tuesday"
                    @if($schedule->tuesday)checked="checked"@endif disabled
>
                    <label class="form-check-label" for="tuesday">
                        Tuesday
                    </label>
                </div>
                <div class="ml-5">
                    <input class="form-check-input" type="checkbox"  name="wednesday" value="4" id="wednesday"
                    @if($schedule->wednesday)checked="checked"@endif disabled
>
                    <label class="form-check-label" for="wednesday">
                        Wednesday
                    </label>
                </div>                            
                <div class="ml-5">
                    <input class="form-check-input" type="checkbox"   name="thursday" value="5" id="thursday"
                    @if($schedule->thursday)checked="checked"@endif disabled
>
                    <label class="form-check-label" for="thursday">
                        Thrusday
                    </label>
                </div>                            
                <div class="ml-5">
                    <input class="form-check-input" type="checkbox"  name="friday" value="6" id="friday"
                    @if($schedule->friday)checked="checked"@endif disabled
>
                    <label class="form-check-label" for="friday">
                        Friday
                    </label>
                </div>
                <div class="ml-5">
                    <input class="form-check-input" type="checkbox"  name="saturday" value="7" id="saturday"
                    @if($schedule->saturday)checked="checked"@endif disabled
>
                    <label class="form-check-label" for="saturday">
                        Saturday
                    </label>
                </div>
                

            </div>

            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-lg-6">

            <div class="card-body contact text-center h-100 white-text">

                <h3 class="my-4 pb-2">Open Hours</h3>
                <ul class="text-lg-left list-unstyled ml-4">
                    @if($schedule->stating_time==null || $schedule->stating_time==null)
                    <p class="text-center">24 Hours</p>
                    @else
                <li>
                    <p>Start: <i class="fa fa-clock pr-2"></i>{{$schedule->stating_time}}</p>
                </li>
                <li>
                    <p>End: <i class="fa fa-clock pr-2"></i>{{$schedule->end_time}}</p>
                </li>
                @endif
                </ul>
                <hr class="hr-light my-4">
                

            </div>

            </div>
            <!-- Grid column -->

        </div>
        <!-- Grid row -->

        </div>
        <!-- Form with header -->

        </section>
        <!-- Section: Contact v.3 -->
                
                </div>
        <!-- Orginfo v.3 -->


        <h4 class="text-center">Items/Employee</h4>
    
    @foreach($items->all() as $item)
    <!--Table    -->
      <div class="container z-depth-1">
          <div class="row">
              <div class="col-md-12">
                  <div class="row">
                      <div class="col-md-3 table_left">
                          @if($item->item_image1)
                          <img class="img-fluid" src="{{$item->item_image1}}" alt="">
                          @else
                          <img class="img-fluid" src="image/x.jpg" alt="">
                          @endif
                      </div>
                      <div class="col-md-9 table_right">
                          <div class="row">
                              <div class="col-md-12">
                                  <h5>{{$item->item_title}}</h5>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <p class="color">Status : 
                                  @if($item->status==1)
                                  <span class="text-success">Available</span>
                                  @else
                                  <span class="text-danger">Not Available</span>
                                  @endif
                                  </p>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <p class="color">Price : 
                                  @if($item->item_price)
                                  <span class="text-success">{{$item->item_price}}</span>
                                  @else
                                  <span class="text-danger">Not Fixed</span>
                                  @endif
                                  </p>
                              </div>
                          </div>
                          <div class="row">
                              <div class="col-md-12">
                                  <p class="color">Info : 
                                  @if($item->item_info)
                                  <span class="text-success">{{$item->item_info}}</span>
                                  @else
                                  <span class="text-danger">Not Avalaible</span>
                                  @endif
                                  </p>
                              </div>
                          </div>
                          <div class="row">
                             
                              <div class="col-md-4 offset-md-8">
                              <div class="form-check radio-green">
                                
                            </div>
                                  <button type="button" class="btn btn-outline-primary "><input class="form-check-input" 
                                  name="booked_item_id" type="radio" id="radio{{$item->id}}" value="{{$item->id}}" @if($item->status==0)disabled @endif>
                                <label class="form-check-label" for="radio{{$item->id}}">Select</label></button>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
      <br>
      @endforeach

      
      
<!---Booking-->
   <br>
   
<!--Form-->
    <h4 class="text-center">Confirm your reservation</h4>
		<div class="container ">
            <!-- Section: Contact v.3 -->
<section class="contact-section my-5">

<!-- Form with header -->
<div class="card">

  <!-- Grid row -->
  <div class="row">

    <!-- Grid column -->
    <div class="col-lg-8">

      <div class="card-body form">


        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-md-6">
            <div class="md-form mb-0">
              <input type="text" name="booked_user_name" id="form-contact-name"value="{{ old('booked_user_name') }}" class="form-control"required="required" >
              <label for="form-contact-name" class="">Your name</label>
            </div>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-6">
            <div class="md-form mb-0">
              <input type="email" name="booked_user_email"  id="form-contact-email" class="form-control">
              <label for="form-contact-email" class="">Your email</label>
            </div>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-md-6">
            <div class="md-form mb-0">
              <input type="number" name="booked_user_phone"value="{{ old('booked_user_phone') }}"  id="form-contact-phone" class="form-control"required="required" >
              <label for="form-contact-phone" class="">Your phone</label>
            </div>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-md-6">
            <div class="md-form mb-0">
              <input type="text" name="booked_user_company	"  id="form-contact-company" class="form-control">
              <label for="form-contact-company" class="">Your company</label>
            </div>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-md-12">
            <div class="md-form mb-0">
              <textarea type="text" name="booked_user_message"  id="form-contact-message" class="form-control md-textarea" rows="3"></textarea>
              <label for="form-contact-message">Your message</label>
              <input type="hidden" name="organization_id" value="{{$organization->id}}">
              <input type="hidden" name="owner_id"  value="{{$organization->user_id}}">
              <button type="submit" class="btn-floating btn-lg blue">
                <i class="fa fa-send-o"></i>
              </button>
            </div>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>

    </div>
    <!-- Grid column -->

    <!-- Grid column -->
    <div class="col-lg-4">

      <div class="card-body contact text-center h-100 white-text">

        <h3 class="my-4 pb-2">Our information</h3>
        <ul class="text-lg-left list-unstyled ml-4">
          <li>
            <p><i class="fa fa-map-marker pr-2"></i>{{$organization->organization_address}}</p>
          </li>
          <li>
            <p><i class="fa fa-phone pr-2"></i>{{$organization->organization_contact}}</p>
          </li>
        </ul>
        <hr class="hr-light my-4">
        

      </div>

    </div>
    <!-- Grid column -->

  </div>
  <!-- Grid row -->

</div>
<!-- Form with header -->

</section>
<!-- Section: Contact v.3 -->
          
		</div>
  </form>
    </div>


@endsection
@section('script')
<script>
	$(document).ready(function(){
		
		$('#readdata').click(function(){
            $("#booking_guests").val($("#booking_guests2").val());
            $("#booking_time").val($("#booking_time2").val());
            $("#booking_date").val($("#booking_date2").val());
               
		});
    });


</script>
@endsection

