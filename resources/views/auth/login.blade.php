@extends('layouts.auth')

@section('content')
<div class="container ">
    <div class="row justify-content-center">
        <div class="col-md-5 ">
            <section class="form-elegant">
            <!-- extra upperspace -->
            <div class="d-none d-md-block" style="margin-top:100px;">
                </div>        
                <!--Form without header-->
                <div class="d-none d-md-block" style="margin-top:20px;">
                </div>
                <div class="card " style="margin-top:30px;">
            
                    <div class="card-body mx-4">
            
                        <!--Header-->
                        <div class="form-header blue">
                        <h3>
                        <i class="fa fa-lock"></i> Login</h3>
                        </div>
                        <form method="POST" action="{{ route('login') }}">
                            @csrf

                        <div class="md-form">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <input id="Form-email1" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            <label for="Form-email1">Your email</label>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="md-form pb-3">
                            <i class="fa fa-lock prefix grey-text"></i>
                            <input id="Form-pass1" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <label for="Form-pass1">Your password</label>
                            <div class="form-check my-4">
                                <input type="checkbox" id="defaultCheck12" class="form-check-input" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="defaultCheck12" class="grey-text">Remember Me</label>
                            </div>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                            <p class="font-small blue-text d-flex justify-content-end">Forgot <a href="password/reset" class="blue-text ml-1"> Password?</a></p>
                        </div>


                        <div class="text-center mb-3">
                            <button type="submit" class="btn blue-gradient btn-block btn-rounded z-depth-1a">Sign in</button>
                        </div>
                        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or Sign in with:</p>
            
                        <div class="row my-3 d-flex justify-content-center">
                            <!--Facebook-->
                            <a href="{{url('login/facebook')}}" class="btn btn-white btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-facebook blue-text text-center"></i></a>
                            <!--Google +-->
                            <a href="{{url('login/google')}}" class="btn btn-white btn-rounded z-depth-1a"><i class="fa fa-google-plus blue-text"></i></a>
                        </div>
        
                    </div>
                    <!--Footer-->
                    <div class="modal-footer mx-5 pt-3 mb-1">
                        <p class="font-small grey-text d-flex justify-content-end">Not a member? <a href="{{ route('register') }}" class="blue-text ml-1"> Sign Up</a></p>
                    </div>

                </div>
                <!--/Form without header-->

            </section>
            
        </div>
    </div>
</div>
@endsection
