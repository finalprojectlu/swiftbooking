@extends('layouts.signup')

@section('content')
<div class="container ">
    <div class="row justify-content-center">
        <div class="col-md-5 ">
            <section class="form-gradient">
            <!-- extra upperspace -->
            <div class="d-none d-md-block" style="margin-top:100px;">
                </div>        
                <!--Form without header-->
                <div class="d-none d-md-block" style="margin-top:20px;">
                </div>
                <div class="card " style="margin-top:30px;">
            
                    <div class="card-body mx-4">
            
                        <!--Header-->
                        <div class="form-header warning-color">
                        <h3>
                        <i class="fa fa-lock"></i> Sign Up</h3>
                        </div>
                        <form method="POST" class="" action="{{ route('register') }}">
                            @csrf

                            <div class="md-form">
                            <i class="fa fa-user prefix grey-text"></i>
                            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            <label for="name">Your name</label>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="md-form">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} orange-text" name="email" value="{{ old('email') }}" required>
                            <label for="email">Your email</label>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="md-form">
                        <i class="fa fa-lock prefix grey-text"></i>
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                            <label for="password">Your password</label>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="md-form pb-3">
                            <i class="fa fa-lock prefix grey-text"></i>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            <label for="password-confirm">Confirm password</label>
                        </div>


                        <div class="text-center mb-3">
                            <button type="submit" class="btn peach-gradient btn-block btn-rounded z-depth-1a">Sign UP</button>
                        </div>
                        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> or Sign in with:</p>
            
                        <div class="row my-3 d-flex justify-content-center">
                            <!--Facebook-->
                            <a href="{{url('login/facebook')}}" class="btn btn-white btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-facebook orange-text text-center"></i></a>
                            <!--Google +-->
                            <a href="{{url('login/google')}}" class="btn btn-white btn-rounded z-depth-1a"><i class="fa fa-google-plus orange-text"></i></a>
                        </div>
        
                    </div>
                    <!--Footer-->
                    <div class="modal-footer mx-5 pt-3 mb-1">
                        <p class="font-small grey-text d-flex justify-content-end">Already a member? <a href="{{ route('login') }}" class="orange-text ml-1"> Sign In</a></p>
                    </div>

                </div>
                <!--/Form without header-->

            </section>
            
        </div>
    </div>
</div>
@endsection
