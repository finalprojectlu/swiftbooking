@extends('layouts.user')

@section('user')

<div class="container">
	<h4 class="text-center"> <b>My Organizations </b></h4>
	<!--Projects section v.4-->
	<section class="text-center mb-5">
		<!--Grid row-->
		<div class="row mb-4">
			<!--Grid column-->
			@foreach($organizations->all() as $organziation)
			<?php
				$i=$organziation->id;
				$i=$i%3;
				if($i==0)
					$color="rgba-blue-strong";
				elseif($i==1)
					$color="rgba-teal-strong";
				elseif($i==2)
					$color="rgba-green-strong";
				elseif($i==3)
					$color="rgba-stylish-strong";
				else
				$color="rgba-stylish-strong";


				$i++;
				$i=$i%3;
			?>
			<!-- Grid column -->
			<div class="col-lg-4 col-md-6 mt-3">
				<div class="card card-body">
					<!-- Featured image -->
					<div class="view overlay rounded z-depth-2 mb-4">
					@if($organziation->organization_cover)
						<img class="img-fluid" src="{{$organziation->organization_cover}}" alt="Sample image">
					@else
						<img class="img-fluid" src="{{asset('img/organization.jpg')}}" alt="Sample image">
					@endif
						<a>
							<div class="mask rgba-white-slight"></div>
						</a>
					</div>

					<!-- Category -->
					<a href="#!" class="deep-orange-text"><h6 class="font-weight-bold mb-3"><i class="{{$organziation->category_icon}} pr-2"></i>{{$organziation->category_title}}</h6></a>
					<!-- Post title -->
					<h4 class="font-weight-bold mb-3"><strong>{{$organziation->organization_name}}</strong></h4>
					<!-- Post data -->
					<p><a class="font-weight-bold">Created on </a>{{date('M j, Y H:i', strtotime($organziation->created_at))}}</p>
					<!-- Excerpt -->
					<p class="dark-grey-text">{{$organziation->organization_info}}</p>
					<!-- Read more button -->
					<a href="{{url("organization/edit/{$organziation->id}")}}" class="btn btn-default btn-rounded btn-md">View Organization</a>

				</div>
			<!-- Grid column -->
			</div>
			<!--Grid column -->
			@endforeach

		
		</div>
		<!--Grid row-->

	</section>
		<!--Section: Tabs-->

</div>
@endsection
