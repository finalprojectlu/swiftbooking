@extends('layouts.organization')

@section('organization')

<div class="container">
    
    <!-- Card -->
    <div class="card card-cascade wider reverse">

        <!-- Card image -->
        <div class="view overlay">
            <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Slides/img%20(70).jpg" alt="Card image cap">
            <a href="#!">
                <div class="mask rgba-white-slight"></div>
            </a>
        </div>
    
        <!-- Card content -->
        <div class="card-body text-center">
    
            <!-- Title -->
            <h4 class="card-title"><strong>{{$organization->organization_name }}</strong></h4>
            <!-- Subtitle -->
            <h6 class="font-weight-bold indigo-text py-2">{{$organization->organization_category }}</h6>
            <!-- Text -->
            <p class="card-text">{{$organization->organization_info }}
            </p>
        
            <!-- Linkedin -->
            <a class="px-2 fa-lg li-ic"><i class="fa fa-linkedin"></i></a>
            <!-- Twitter -->
            <a class="px-2 fa-lg tw-ic"><i class="fa fa-twitter"></i></a>
            <!-- Dribbble -->
            <a class="px-2 fa-lg fb-ic"><i class="fa fa-facebook"></i></a>
    
        </div>
        
    </div>
    <!-- Card -->
                  
    
    <!-- Card -->
    <div class="card card-cascade narrower mt-5">

        <!-- Card image -->
        <div class="view gradient-card-header mb-4 p-4 info-color">

            <!-- Title -->
            <h2 class="card-header-title">Make <strong>Section</strong>
            </h2>
            <p>Employee/Item</p>
            


        </div>
        <form method="POST" action="{{ route('add_organization') }}">
        <div class="row p-3" >
        
            <div class="col-lg-12 mt-2" >
               <div class="card card-body ">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4 class="font-weight-bold pl-0 my-4"><strong>Basic Information</strong></h4>
                            <div class="form-group md-form">
                                <input id="item_title" name="item_title[]" type="text" required="required" class="form-control validate">
                                <label for="item_title">Title</label>
                                @if ($errors->has('item_title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="item_link">Link</label>
                                <input id="item_link" name="item_link[]" type="text" required="required" class="form-control validate">
                                @if ($errors->has('item_link'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_link') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <input id="item_price" name="item_price[]" type="text" required="required" class="form-control validate">
                                <label for="item_price">Price</label>
                                @if ($errors->has('item_price'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_price') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="item_info">Info</label>
                                <textarea  id="item_info" name="item_info[]" class="form-control md-textarea validate" row="2"></textarea>
                                @if ($errors->has('item_info'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_info') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 1</span>
                                            <input name="photo_1[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 2</span>
                                            <input name="photo_2[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>

                        </div>
                        <div class="col-lg-6">
                        <h4 class="font-weight-bold pl-0 my-4"><strong>Schudle</strong></h4>
                                <h5>Available Days'</h5>
                                <!-- Default checkbox -->
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="day_sunday[]" value="1" id="day_sunday[]">
                                    <label class="form-check-label" for="day_sunday[]">
                                        Sunday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_monday[]" value="2" id="day_monday[]">
                                    <label class="form-check-label" for="day_monday[]">
                                        Monday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_tuesday[]" value="3" id="day_tuesday[]">
                                    <label class="form-check-label" for="day_tuesday[]">
                                        Tuesday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_wednesday[]" value="4" id="day_wednesday[]">
                                    <label class="form-check-label" for="day_wednesday[]">
                                        Wednesday
                                    </label>
                                </div>                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"   name="day_thursday[]" value="5" id="day_thursday[]">
                                    <label class="form-check-label" for="day_thursday[]">
                                        Thrusday
                                    </label>
                                </div>                            <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_friday[]" value="6" id="day_friday[]">
                                    <label class="form-check-label" for="day_friday[]">
                                        Friday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_saturday[]" value="7" id="day_saturday[]">
                                    <label class="form-check-label" for="day_saturday[]">
                                        Saturday
                                    </label>
                                </div>
                                <hr/>
                                <!-- Grid row -->
                                <h5>Open Hours</h5>

                                <div class="row">
                                    <!-- Grid column -->
                                    <div class="col">
                                        <input placeholder="Statring time" name="starting_time[]" type="text" id="input_starttime" class="form-control timepicker">
                                    </div>
                                    <!-- Grid column -->

                                    <!-- Grid column -->
                                    <div class="col">
                                        <input placeholder="Ending time" name="ending_time[]" type="text" id="input_starttime" class="form-control timepicker">
                                    </div>
                                    <!-- Grid column -->
                                </div>
                                <!-- Grid row -->
                                <div class="form-group md-form mt-3">
                                    <label for="remarks[]" data-error="wrong" data-success="right">Remarks</label>
                                    <textarea id="remarks[]" name="remarks[]" type="text" required="required" rows="2" class="md-textarea validate form-control"></textarea>
                                </div>
                        </div>
                        <div class>
                        </div>

                    </div>
                </div>
            </div>

            <div class="col-lg-6 mt-2" >
 
                <!-- Card content -->
                <div class="card card-body fixedcardsize">
                    <!-- Stepper -->
                    <div class="steps-form-2 mt-3">
                        <div class="steps-row-2 setup-panel-2 d-flex justify-content-between">
                            <div class="steps-step-2">
                                <a href="#step-1" type="button" class="btn btn-amber btn-circle-2 waves-effect ml-0" data-toggle="tooltip" data-placement="top" title="Basic Information"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                            </div>
                            <div class="steps-step-2">
                                <a href="#step-2" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect" data-toggle="tooltip" data-placement="top" title="Personal Data"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                            </div>
                            <div class="steps-step-2">
                                <a href="#step-3" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect" data-toggle="tooltip" data-placement="top" title="Terms and Conditions"><i class="fa fa-photo" aria-hidden="true"></i></a>
                            </div>
                            <div class="steps-step-2">
                                <a href="#step-4" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect mr-0" data-toggle="tooltip" data-placement="top" title="Finish"><i class="fa fa-check" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
        
                    <!-- First Step -->

                        @csrf
                        <div class="row setup-content-2" id="step-1">
                            
                            <div class="col-md-12 ">
                                <h3 class="font-weight-bold pl-0 my-4"><strong>Basic Information</strong></h3>
                                <div class="form-group md-form">
                                    <input id="item_title" name="item_title[]" type="text" required="required" class="form-control validate">
                                    <label for="item_title">Title</label>
                                    @if ($errors->has('item_title'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('item_title') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group md-form">
                                    <label for="item_link">Link</label>
                                    <input id="item_link" name="item_link[]" type="text" required="required" class="form-control validate">
                                    @if ($errors->has('item_link'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('item_link') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group md-form">
                                    <input id="item_price" name="item_price[]" type="text" required="required" class="form-control validate">
                                    <label for="item_price">Price</label>
                                    @if ($errors->has('item_price'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('item_price') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group md-form">
                                    <label for="item_info">Info</label>
                                    <textarea  id="item_info" name="item_info[]" class="form-control md-textarea validate" row="2"></textarea>
                                    @if ($errors->has('item_info'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('item_info') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
                            </div>
                        </div>
        
                    <!-- Second Step -->
                        <div class="row setup-content-2" id="step-2">
                            <div class="col-md-12">
                                <h3 class="font-weight-bold pl-0 my-4"><strong>Schudle</strong></h3>
                                <h5>Available Days'</h5>
                                <!-- Default checkbox -->
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="day_sunday[]" value="1" id="day_sunday[]">
                                    <label class="form-check-label" for="day_sunday[]">
                                        Sunday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_monday[]" value="2" id="day_monday[]">
                                    <label class="form-check-label" for="day_monday[]">
                                        Monday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_tuesday[]" value="3" id="day_tuesday[]">
                                    <label class="form-check-label" for="day_tuesday[]">
                                        Tuesday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_wednesday[]" value="4" id="day_wednesday[]">
                                    <label class="form-check-label" for="day_wednesday[]">
                                        Wednesday
                                    </label>
                                </div>                            
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"   name="day_thursday[]" value="5" id="day_thursday[]">
                                    <label class="form-check-label" for="day_thursday[]">
                                        Thrusday
                                    </label>
                                </div>                            <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_friday[]" value="6" id="day_friday[]">
                                    <label class="form-check-label" for="day_friday[]">
                                        Friday
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox"  name="day_saturday[]" value="7" id="day_saturday[]">
                                    <label class="form-check-label" for="day_saturday[]">
                                        Saturday
                                    </label>
                                </div>
                                <hr/>
                                <!-- Grid row -->
                                <h5>Open Hours</h5>

                                <div class="row">
                                    <!-- Grid column -->
                                    <div class="col">
                                        <input placeholder="Statring time" name="starting_time[]" type="text" id="input_starttime" class="form-control timepicker">
                                    </div>
                                    <!-- Grid column -->

                                    <!-- Grid column -->
                                    <div class="col">
                                        <input placeholder="Ending time" name="ending_time[]" type="text" id="input_starttime" class="form-control timepicker">
                                    </div>
                                    <!-- Grid column -->
                                </div>
                                <!-- Grid row -->
                                <div class="form-group md-form mt-3">
                                    <label for="remarks[]" data-error="wrong" data-success="right">Remarks</label>
                                    <textarea id="remarks[]" name="remarks[]" type="text" required="required" rows="2" class="md-textarea validate form-control"></textarea>
                                </div>
                                <button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
                                <button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
                            </div>
                        </div>
        
                        <!-- Third Step -->
                        <div class="row setup-content-2" id="step-3">
                            <div class="col-md-12">
                                <h3 class="font-weight-bold pl-0 my-4"><strong>Photo Gallery</strong></h3>
                                <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 1</span>
                                            <input name="photo_1[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 2</span>
                                            <input name="photo_2[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>

                                <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 3</span>
                                            <input name="photo_3[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload your file">
                                        </div>
                                    </div>
                                </div>

                                <button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
                                <button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
                            </div>
                        </div>
        
                        <!-- Fourth Step -->
                        <div class="row setup-content-2" id="step-4">
                            <div class="col-md-12">
                                <h3 class="font-weight-bold pl-0 my-4"><strong>Finish</strong></h3>
                                <h2 class="text-center font-weight-bold my-4">Registration completed!</h2>
                                <button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
                                <button class="btn btn-success btn-rounded float-right" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                    
            
                </div>
                    
            </div>
            <div  id="dynamic_sector">
            </div>
            <div class="col-lg-6 mt-2">
                <div class="card card-body fixedcardsize">
                        <a type="button" name="add" id="add">
                            <!-- <i class="fa fa-camera-retro fa-5x"></i> -->
                            <img class="img-fluid" src="{{asset('img/plus.jpg')}}" />
                            <h1 class="text-center">Add more sections</h1>
                            

                        </a>

                </div>
            </div>

        </div>

    </div>
    <!-- Card -->

</div>



<!-- hidden append -->
<div class="hidden" id="forappend">
<div class="col-lg-6 mt-2" >
<script>document.write(i)</script>
<!-- Card content -->
<div class="card card-body fixedcardsize">
    <!-- Stepper -->
    <div class="steps-form-2 mt-3">
        <div class="steps-row-2 setup-panel-2 d-flex justify-content-between">
            <div class="steps-step-2">
                <a href="#step-1" type="button" class="btn btn-amber btn-circle-2 waves-effect ml-0" data-toggle="tooltip" data-placement="top" title="Basic Information"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
            </div>
            <div class="steps-step-2">
                <a href="#step-2" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect" data-toggle="tooltip" data-placement="top" title="Personal Data"><i class="fa fa-pencil" aria-hidden="true"></i></a>
            </div>
            <div class="steps-step-2">
                <a href="#step-3" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect" data-toggle="tooltip" data-placement="top" title="Terms and Conditions"><i class="fa fa-photo" aria-hidden="true"></i></a>
            </div>
            <div class="steps-step-2">
                <a href="#step-4" type="button" class="btn btn-blue-grey btn-circle-2 waves-effect mr-0" data-toggle="tooltip" data-placement="top" title="Finish"><i class="fa fa-check" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>

    <!-- First Step -->
        <div class="row setup-content-2" id="step-1">
            
            <div class="col-md-12 ">
                <h3 class="font-weight-bold pl-0 my-4"><strong>Basic Information</strong></h3>
                <div class="form-group md-form">
                    <input id="item_title" type="text" required="required" class="form-control validate">
                    <label for="item_title">Title</label>
                    @if ($errors->has('item_title'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('item_title') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group md-form">
                    <label for="item_link">Link</label>
                    <input id="item_link" type="text" required="required" class="form-control validate">
                    @if ($errors->has('item_link'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('item_link') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group md-form">
                    <input id="item_price" type="text" required="required" class="form-control validate">
                    <label for="item_price">Price</label>
                    @if ($errors->has('item_price'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('item_price') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group md-form">
                    <label for="item_info">Info</label>
                    <textarea  id="item_info" class="form-control md-textarea validate" row="2"></textarea>
                    @if ($errors->has('item_info'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('item_info') }}</strong>
                        </span>
                    @endif
                </div>

                <button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
            </div>
        </div>

    <!-- Second Step -->
        <div class="row setup-content-2" id="step-2">
            <div class="col-md-12">
                <h3 class="font-weight-bold pl-0 my-4"><strong>Schudle</strong></h3>
                <h5>Available Days'</h5>
                <!-- Default checkbox -->
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                    <label class="form-check-label" for="defaultCheckbox1">
                        Sunday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                    <label class="form-check-label" for="defaultCheckbox1">
                        Monday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                    <label class="form-check-label" for="defaultCheckbox1">
                        Tuesday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                    <label class="form-check-label" for="defaultCheckbox1">
                        Wednesday
                    </label>
                </div>                            
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                    <label class="form-check-label" for="defaultCheckbox1">
                        Thrusday
                    </label>
                </div>                            <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                    <label class="form-check-label" for="defaultCheckbox1">
                        Friday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="defaultCheckbox1">
                    <label class="form-check-label" for="defaultCheckbox1">
                        Saturday
                    </label>
                </div>
                <hr/>
                <!-- Grid row -->
                <h5>Open Hours</h5>

                <div class="row">
                    <!-- Grid column -->
                    <div class="col">
                        <input placeholder="Statring time" type="text" id="input_starttime" class="form-control timepicker">
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col">
                        <input placeholder="Ending time" type="text" id="input_starttime" class="form-control timepicker">
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
                <div class="form-group md-form mt-3">
                    <label for="yourAddress-2" data-error="wrong" data-success="right">Remarks</label>
                    <textarea id="yourAddress-2" type="text" required="required" rows="2" class="md-textarea validate form-control"></textarea>
                </div>
                <button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
                <button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
            </div>
        </div>

        <!-- Third Step -->
        <div class="row setup-content-2" id="step-3">
            <div class="col-md-12">
                <h3 class="font-weight-bold pl-0 my-4"><strong>Photo Gallery</strong></h3>
                <div  class="dropzone"></div>
                <button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
                <button class="btn btn-mdb-color btn-rounded nextBtn-2 float-right" type="button">Next</button>
            </div>
        </div>

        <!-- Fourth Step -->
        <div class="row setup-content-2" id="step-4">
            <div class="col-md-12">
                <h3 class="font-weight-bold pl-0 my-4"><strong>Finish</strong></h3>
                <h2 class="text-center font-weight-bold my-4">Registration completed!</h2>
                <button class="btn btn-mdb-color btn-rounded prevBtn-2 float-left" type="button">Previous</button>
                <button class="btn btn-success btn-rounded float-right" type="submit">Submit</button>
            </div>
        </div>
    </form>
    

</div>
    
</div>
</div>
 
@endsection
