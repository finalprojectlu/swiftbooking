@extends('layouts.organization')

@section('organization')

<div class="container">
<!-- form -->
<form method="POST" action="{{ route('finishorganizationseat') }}" enctype="multipart/form-data">
    @csrf
    
    <!-- Card -->
    <div class="card card-cascade wider reverse">
    
        <!-- Card image -->
        <div class="view overlay">
        @if($organization->organization_cover)
            <img class="card-img-top" src="{{$organization->organization_cover}}" alt="Card image cap">
            @endif
            <a href="#!">
                <div class="mask rgba-white-slight"></div>
            </a>
        </div>
    
        <!-- Card content -->
        <div class="card-body ">
        <div class="text-center">
    
        <!-- Title -->
        <div class="foo">
                <span class="letter" data-letter="{{$organization->organization_name }}">{{$organization->organization_name }}</span>
            </div>
            <!-- Subtitle -->
            <h6 class="font-weight-bold indigo-text py-2">{{$category->category_title}}</h6>
            <!-- Text -->
            <p class="card-text">{{$organization->organization_info }}
            </p>
            <h4 class=" pl-0 my-4">Set <strong>Map Image</strong></h4>
            <div class="col-lg-8 offset-lg-2">
                <div class="md-form">
                    <div class="file-field">
                        <div class="btn btn-mdb-color btn-rounded float-left">
                            <span>Set Image</span>
                            <input name="organization_graphical" type="file">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Upload Image">
                        </div>
                    </div>
                </div>
            </div>
            <h4 class=" pl-0 my-4">Set <strong>Schudle</strong></h4>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <h4>Available Days'</h4>
                <!-- Default checkbox -->
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="sunday" value="1" id="sunday">
                    <label class="form-check-label" for="sunday">
                        Sunday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="monday" value="2" id="monday" checked="checked">
                    <label class="form-check-label" for="monday">
                        Monday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="tuesday" value="3" id="tuesday" checked="checked">
                    <label class="form-check-label" for="tuesday">
                        Tuesday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="wednesday" value="4" id="wednesday" checked="checked">
                    <label class="form-check-label" for="wednesday">
                        Wednesday
                    </label>
                </div>                            
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"   name="thursday" value="5" id="thursday" checked="checked">
                    <label class="form-check-label" for="thursday">
                        Thrusday
                    </label>
                </div>                            
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="friday" value="6" id="friday" checked="checked">
                    <label class="form-check-label" for="friday">
                        Friday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="saturday" value="7" id="saturday" checked="checked">
                    <label class="form-check-label" for="saturday">
                        Saturday
                    </label>
                </div>
            </div>
            <div class="col-lg-6">
            <!-- Grid row -->
            <h4>Open Hours</h4>
                <div class="row">
                    <!-- Grid column -->
                    <div class="col">
                        <input placeholder="Statring time" name="starting_time" type="text" id="starting_time" class="form-control timepicker">
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col">
                        <input placeholder="Ending time" name="ending_time" type="text" id="ending_time" class="form-control timepicker">
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
                <div class="form-group md-form mt-3">
                    <label for="remarks" data-error="wrong" data-success="right">Remarks</label>
                    <textarea id="remarks" name="remarks" type="text"rows="2" class="md-textarea validate form-control"></textarea>
                </div>
            </div>

        </div>
    
        </div>
        
    </div>
    <!-- Card -->
                  
    
    <!-- Card -->
    <div class="card card-cascade narrower mt-5">

        <!-- Card image -->
        <div class="view gradient-card-header mb-4 p-4 info-color">

            <!-- Title -->
            <h2 class="card-header-title">Make <strong>Section</strong>
            </h2>
            <p>Employee/Item</p>
            


        </div>
        <div class="row p-3" >
        
            <div class="col-lg-6 mt-2 " >
               <div class="card card-body fixedcardsize">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class=" pl-0 my-4"><strong>Basic Information</strong></h4>
                            <div class="form-group md-form">
                                <input id="item_title" name="item_title[]" type="text" required="required" class="form-control validate">
                                <label for="item_title">Name or Number *</label>
                                @if ($errors->has('item_title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="limit">Stock *</label>
                                <input id="limit" name="limit[]" type="text" value="1" required="required"  class="form-control validate">
                                @if ($errors->has('limit'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('limit') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <input id="item_price" name="item_price[]" type="text" class="form-control validate">
                                <label for="item_price">Fee</label>
                                @if ($errors->has('item_price'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_price') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="item_info">Info</label>
                                <textarea  id="item_info" name="item_info[]" class="form-control md-textarea validate" row="2"></textarea>
                                @if ($errors->has('item_info'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_info') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 1</span>
                                            <input name="item_image1[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload Image">
                                        </div>
                                    </div>
                                </div>

                                <div class="hidden">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 2</span>
                                            <input name="item_image2[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate"  type="text" placeholder="Upload Image">
                                        </div>
                                    </div>
                                </div>

                        </div>
                        

                    </div>
                </div>
            </div>
            <div  id="dynamic_sector">
            </div>
            <div class="col-lg-6 mt-2">
                <div class="card card-body fixedcardsize">
                    <a type="button" name="add" id="add">
                        <!-- <i class="fa fa-camera-retro fa-5x"></i> -->
                        <img class="img-fluid" src="{{asset('img/plus.jpg')}}" />
                        <h1 class="text-center">Add more sections</h1>
                        

                    </a>

                </div>
            </div>
            <div class="col-lg-12 m-3">
                <div class="row">
                    <div class=" col-lg-6 offset-2">
                        <input type="hidden" name="organization_id" value="{{$organization->id}}">
                        <button type="submit"  class="btn btn-outline-primary btn-rounded waves-effect btn-lg btn-block">
                            <b>Submit</b>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
    <!-- Card -->
    </form>
</div>

<!-- hidden append -->
<div class="hidden" id="forappend">
<div class="col-lg-6 mt-2 " >
               <div class="card card-body fixedcardsize">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class=" pl-0 my-4"><strong>Basic Information</strong></h4>
                            <div class="form-group md-form">
                                <input id="item_title" name="item_title[]" type="text" required="required" class="form-control validate">
                                <label for="item_title">Name or Number *</label>
                                @if ($errors->has('item_title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="limit">Stock *</label>
                                <input id="limit" name="limit[]" type="text" value="1" required="required"  class="form-control validate">
                                @if ($errors->has('limit'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('limit') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <input id="item_price" name="item_price[]" type="text" class="form-control validate">
                                <label for="item_price">Fee</label>
                                @if ($errors->has('item_price'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_price') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="item_info">Info</label>
                                <textarea  id="item_info" name="item_info[]" class="form-control md-textarea validate" row="2"></textarea>
                                @if ($errors->has('item_info'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_info') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 1</span>
                                            <input name="item_image1[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" type="text" placeholder="Upload Image">
                                        </div>
                                    </div>
                                </div>

                                <div class="hidden">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 2</span>
                                            <input name="item_image2[]" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate"  type="text" placeholder="Upload Image">
                                        </div>
                                    </div>
                                </div>

                        </div>
                        

                    </div>
                </div>
            </div>
</div>
 
@endsection
