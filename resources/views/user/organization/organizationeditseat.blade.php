@extends('layouts.organization')

@section('organization')

<div class="container">
@include('layouts.alart')
    
    <!-- Card -->
    <div class="card card-cascade align-middle wider reverse">
    
        <!-- Card image -->
        <div class="view overlay ">
        @if($organization->organization_cover)
            <img class="card-img-top img-fluid mx-auto d-blockr" style=" max-height:400px;width:auto;" src="{{$organization->organization_cover}}" alt="Card image cap">
        @endif
                <a href="#!">
                <div class="mask rgba-white-slight"></div>
            </a>
        </div>
    
        <!-- Card content -->
        <div class="card-body ">
            <!-- form -->
        <!-- form -->
        <form method="POST" action="{{ route('organizationupdate') }}" enctype="multipart/form-data">
        @csrf
        <div class="text-center">
            <!-- Title -->
            <div class="foo">
                <span class="letter" data-letter="{{$organization->organization_name }}">{{$organization->organization_name }}</span>
            </div>
            <!-- Subtitle -->
            <h6 class="font-weight-bold indigo-text py-2">{{$organization->category_title }}</h6>
            <!-- Text -->
            <p class="card-text">{{$organization->organization_info }}
            </p>
        
            
            
            <h4 class=" pl-0 my-4">Set <strong>Map Image</strong></h4>
            <div class="col-lg-8 offset-lg-2">
                <div class="md-form">
                @if($organization->organization_graphical)
                    <img class="img-fluid mx-auto d-blockr" style=" max-height:300px;width:auto;" src="{{$organization->organization_graphical}}" alt="Card image cap">
                @endif
                    <div class="file-field">
                        <div class="btn btn-mdb-color btn-rounded float-left">
                            <span>Set Image</span>

                            <input name="organization_graphical" value="{{$organization->organization_graphical }}" type="file">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" name="organization_graphical"  type="text"  value="{{$organization->organization_graphical }}" placeholder="Upload Image">
                        </div>
                    </div>
                </div>
            </div>
            <h4 class=" pl-0 my-4">Set <strong>Schudle</strong></h4>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="text-left">
                    <div class="md-form">
                        <i class="fa fa-envelope prefix grey-text"></i>
                        <input type="text" value="{{$organization->organization_name }}" name="organization_name" id="organization_name" class="form-control ">
                        <label for="organization_name ">Organization Name</label>
                        @if ($errors->has('organization_name'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('organization_name') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="md-form">
                        <i class="fa fa-envelope prefix grey-text"></i>
                        <input type="text" value="{{$organization->organization_link }}" name="organization_link" id="organization_link" class="form-control ">
                        <label for="organization_link ">Website</label>
                        @if ($errors->has('organization_link'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('organization_link') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="md-form">
                        <i class="fa fa-envelope prefix grey-text"></i>
                        <input type="text"  value="{{$organization->organization_address }}" name="organization_address" id="organization_address" class="form-control ">
                        <label for="organization_address ">Address</label>
                        @if ($errors->has('organization_address'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('organization_address') }}</strong>
                            </span>
                        @endif
                    </div>

                    <!-- Material textarea message -->
                    <div class="md-form form-sm">
                        <i class="fa fa-pencil prefix grey-text"></i>
                        <textarea type="text" id="organization_info" class="md-textarea form-control">{{$organization->organization_info }}</textarea>
                        <label for="organization_info">Info</label>
                        @if ($errors->has('organization_info'))
                            <span class="invalid-feedback">
                                <strong>{{ $errors->first('organization_info') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="md-form">
                        <div class="file-field">
                            <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                <span>Cover Image</span>
                                <input name="organization_cover" type="file">
                            </div>
                            <div class="file-path-wrapper">
                                <input value="{{$organization->organization_cover }}" class="file-path validate" name="organization_cover" type="text" placeholder="Upload Image">
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            <div class="col-lg-6">
            <h4>Available Days'</h4>
                <!-- Default checkbox -->
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="sunday" value="1"  id="sunday"
                    @if($organization->sunday)checked="checked"@endif
                    >
                    <label class="form-check-label" for="sunday">
                        Sunday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="monday" value="2" id="monday"
                    @if($organization->monday)checked="checked"@endif
>
                    <label class="form-check-label" for="monday">
                        Monday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="tuesday" value="3" id="tuesday"
                    @if($organization->tuesday)checked="checked"@endif
>
                    <label class="form-check-label" for="tuesday">
                        Tuesday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="wednesday" value="4" id="wednesday"
                    @if($organization->wednesday)checked="checked"@endif
>
                    <label class="form-check-label" for="wednesday">
                        Wednesday
                    </label>
                </div>                            
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"   name="thursday" value="5" id="thursday"
                    @if($organization->thursday)checked="checked"@endif
>
                    <label class="form-check-label" for="thursday">
                        Thrusday
                    </label>
                </div>                            
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="friday" value="6" id="friday"
                    @if($organization->friday)checked="checked"@endif
>
                    <label class="form-check-label" for="friday">
                        Friday
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox"  name="saturday" value="7" id="saturday"
                    @if($organization->saturday)checked="checked"@endif
>
                    <label class="form-check-label" for="saturday">
                        Saturday
                    </label>
                </div>
            <!-- Grid row -->
            <h4>Open Hours</h4>
                <div class="row">
                    <!-- Grid column -->
                    <div class="col">
                        <input value="{{$organization->starting_time}}" placeholder="Statring time" name="starting_time" type="text" id="starting_time" class="form-control timepicker">
                    </div>
                    <!-- Grid column -->

                    <!-- Grid column -->
                    <div class="col">
                        <input value="{{$organization->ending_time}}"  placeholder="Ending time" name="ending_time" type="text" id="ending_time" class="form-control timepicker">
                    </div>
                    <!-- Grid column -->
                </div>
                <!-- Grid row -->
                <div class="form-group md-form mt-3">
                    <label for="remarks" data-error="wrong" data-success="right">Remarks</label>
                    <textarea id="remarks" name="remarks" type="text"rows="2" class="md-textarea validate form-control">{{$organization->remarks}}</textarea>
                </div>
            </div>
        </div>
        <div class="col-lg-12 m-3">
                <div class="row">
                <div class=" col-lg-6 offset-2">
                <input type="hidden" name="organization_id" value="{{$organization->org_id}}">
                    <input type="hidden" name="schedule_id" value="{{$schedule->id}}">
                        <button type="submit"  class="btn btn-outline-primary btn-rounded waves-effect btn-lg btn-block">
                            <b>Update</b>
                        </button>
                        </div>
                </div>
            </div>
        </form>
        </div>
    
    </div>
    <!-- Card -->
                  
    
    <!-- Card -->
    <div class="card card-cascade  mt-5">

        <!-- Card image -->
        <div class="view gradient-card-header mb-4 p-4 info-color">

            <!-- Title -->
            <h2 class="card-header-title">Section <strong>List</strong>
            </h2>
            


        </div>
        <div class="row p-3" >

               <div class="col-md-12">
                                           
                   <div class="card">
                       <div class="card-body">
                           <table id="datatables" class="table table-striped table-bordered table-responsive-md" cellspacing="0" width="100%">
                               <thead>
                                   <tr>
                                       <th>Title</th>
                                       <th>Total Stock</th>
                                       <th>Fee</th>
                                       <th>Info</th>
                                       <th class="text-center">Action</th>
                                       <th class="text-center">Status</th>
                                   </tr>
                               </thead>
                               <tfoot>
                                   <tr>
                                   <th>Title</th>
                                   <th>Total Stock</th>
                                   <th>Fee </th>
                                   <th>Info</th>
                                   <th class="text-center">Action</th>
                                   <th class="text-center">Status</th>
                               </tr>
                               </tfoot>
                               <tbody>
                               @foreach($items->all() as $item)
                               <tr>
                                    <td>{{ $item->item_title }}</td>
                                    <td>{{ $item->limit }}</td>
                                    <td>{{ $item->item_price }}</td>
                                    <td>{{ $item->item_info }}</td>
                                    <td class="text-center">
                                        <a data-toggle="modal" data-target="#deleteModal{{$item->id}}"><i class="fa fa-remove fa-lg red-text" aria-hidden="true"></i></a>
                                    </td>
                                    
                                    <td class="text-center">
                                    @if( $item->status == 0)
                                    <a href="{{url('/organization/item/enable/'.$item->id)}}"  class="btn btn-success btn-rounded btn-sm">Enabale</a>
                                    @elseif( $item->status == 1)
                                    <a href="{{url('/organization/item/disable/'.$item->id)}}" class="btn btn-danger btn-rounded btn-sm">Disable</a>

                                    @endif
                                    </td>
                                </tr>
                                <!-- Modal -->
                                <div class="modal fade" id="deleteModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Delete " {{ $item->item_title }} "</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure that you want to delete {{ $item->item_title }} permanently, this will not be undo after confermation
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <a href="{{url('/organization/item/delete/'.$item->id)}}" class="btn btn-success">Yes, Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Modal Ends-->
                                @endforeach
                                </tbody>
                           </table>
                       </div>
                   </div>

           </div>
           <div class="col-lg-12 mt-3">
                <div class="card card-body ">
                    <a href=""  data-toggle="modal" data-target="#exampleModal">
                        
                        <h1 class="text-center"><i class="fa fa-plus " aria-hidden="true"></i> Add New Section</h1>
                    </a>
                </div>
            </div>

      

        </div>
        </section>
        
    
    </div>
    <!-- Card -->

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <form method="POST" action="{{ route('addsection') }}" enctype="multipart/form-data">
                @csrf
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add sections</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
            <!-- form -->
            <form method="POST" action="{{ route('addsection') }}" enctype="multipart/form-data">
                @csrf
                            <div class="form-group md-form">
                                <input id="item_title" name="item_title" type="text" required="required" class="form-control validate">
                                <label for="item_title">Title *</label>
                                @if ($errors->has('item_title'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="limit">Stock *</label>
                                <input id="item_llimitink" value="1" name="limit" type="text" class="form-control validate">
                                @if ($errors->has('limit'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('limit') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <input id="item_price" name="item_price" type="text" class="form-control validate">
                                <label for="item_price">Fee</label>
                                @if ($errors->has('item_price'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_price') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group md-form">
                                <label for="item_info">Info</label>
                                <textarea  id="item_info" name="item_info" class="form-control md-textarea validate" row="2"></textarea>
                                @if ($errors->has('item_info'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('item_info') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="md-form">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 1</span>
                                            <input name="item_image1" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" name="photo_1" type="text" placeholder="Upload Image">
                                        </div>
                                    </div>
                                </div>

                                <div class="hidden">
                                    <div class="file-field">
                                        <div class="btn btn-mdb-color btn-rounded btn-sm float-left">
                                            <span>Photo 2</span>
                                            <input name="item_image2" type="file">
                                        </div>
                                        <div class="file-path-wrapper">
                                            <input class="file-path validate" name="photo_2" type="text" placeholder="Upload Image">
                                        </div>
                                    </div>
                                </div>
                            
                        </div>
            </div>
            <div class="modal-footer">
                <input id="organization_id" name="organization_id" type="hidden" value="{{$organization->org_id}}">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Add</button>
            </div>
            </form >
        </div>
    </div>
</div>
 
@endsection
