@extends('layouts.organization')

@section('organization')

<div class="container">
    
    <!-- Card -->
    <div class="card card-cascade narrower">

        <!-- Card image -->
        <div class="view gradient-card-header mb-4 p-4 info-color">

            <!-- Title -->
            <h2 class="card-header-title">Start <strong>Organization</strong></h2>

        </div>

        <!-- Card content -->
        <div class="card-body">
            <div class="row">
                <div class="col-lg-8 offset-2">
                    <form  method="POST" action="{{ route('add_organization') }}" id="uploadForm "  enctype="multipart/form-data">
                    @csrf
                        <div class="file-field">
                            <div class="z-depth-1-half mb-4">
                               
                            </div>
                            <div class="d-flex justify-content-center">
                                <div class="btn btn-mdb-color btn-rounded float-left">
                                    <span>Choose Cover Image</span>
                                    <input type="file" name="organization_cover" id="organization_cover" >
                                </div>
                            </div>
                        </div>
                        <!-- Material input -->
                        <div class="md-form">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <input type="text" name="organization_name" id="organization_name" class="form-control ">
                            <label for="organization_name ">Organization Name</label>
                            @if ($errors->has('organization_name'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('organization_name') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- Material input text -->
                        <div class="md-form">
                            <i class="fa fa-user prefix grey-text"></i>
                            <!--Blue select-->
                            <select  id="organization_category" name="organization_category" 
                            class=" mdb-select colorful-select dropdown-primary ml-5 grey-text">
                                <option value="">Select Option</option>
                                @foreach($categories->all() as $category)
                                <option value="{{ $category->id }}">{{ $category->category_title }}</option>
                                
                                @endforeach
                            </select>
                            <label  for="organization_category" class="font-weight-light">Category</label>
                            <!--/Blue select--> 
                            <!-- <input type="text" id="category_color" name="category_color" class="form-control">
                            <label for="category_color" class="font-weight-light">Category Color</label> -->
                            @if ($errors->has('organization_category'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('organization_category') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!--// Material input text -->

                        <div class="md-form">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <input type="text" name="organization_link" id="organization_link" class="form-control ">
                            <label for="organization_link ">Website</label>
                            @if ($errors->has('organization_link'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('organization_link') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="md-form">
                            <i class="fa fa-envelope prefix grey-text"></i>
                            <input type="text" name="organization_address" id="organization_address" class="form-control ">
                            <label for="organization_address ">Address</label>
                            @if ($errors->has('organization_address'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('organization_address') }}</strong>
                                </span>
                            @endif
                        </div>

                        <!-- Material textarea message -->
                        <div class="md-form form-sm">
                            <i class="fa fa-pencil prefix grey-text"></i>
                            <textarea type="text" id="organization_info" row="2" class="md-textarea form-control"></textarea>
                            <label for="organization_info">Info</label>
                            @if ($errors->has('organization_info'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('organization_info') }}</strong>
                                </span>
                            @endif
                        </div>
                        <div class="text-center py-4 mt-3">
                            <button class="btn btn-outline-default btn-rounded waves-effect btn-lg" type="submit">Done</button>
                        </div>



                    </form>
                </div>
                
                <div class="">
                </div>
            </div>
        </div>
    </div>
    <!-- Card -->

</div>
<script>
Vue.config.debug = true;
		Vue.config.devtools = true
    var app=new Vue({
        el: '#app',
        data:{
                avatar:https://mdbootstrap.com/img/Photos/Others/placeholder.jpg
            
        },
        methods:{
            getimage(e)
            {
                let image=e.target.files[0]
                let reader= new FileReader();
                reader.readAsDataURL(image);
                reader.onload=e=>{
                    this.avatar =e.target.result
                    
                }
                
            },
            upload(){
                axios.post('/upload')
            }
        }
    }


    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#organization_cover + img').remove();
                $('#organization_cover').source(e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#organizaion_cover").change(function () {
    filePreview(this);
});
</script>

@endsection
