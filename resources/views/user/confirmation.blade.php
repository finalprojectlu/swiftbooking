@extends('layouts.organization')

@section('organization')
<div class="container">
    @include('layouts.alart')
    <div class="row justify-content-center">
        <div class="col-md-10 mb-5">
           <!--Section: Basic examples-->
           <section>
           
                           <div class="row">
                               
                               <div class="col-md-12">
                                                           
                                   <div class="card">
                                       <div class="card-body">
                                           <table id="datatables" class="table table-striped table-bordered table-responsive-md" cellspacing="0" width="100%">
                                               <thead>
                                                   <tr>
                                                       <th>Organization</th>
                                                       <th>Section</th>
                                                       <th>Client Name</th>
                                                       <th>Client Contact</th>
                                                       <th class="text-center">View Full Info</th>
                                                       <th class="text-center">Status</th>
                                                   </tr>
                                               </thead>
                                               <tfoot>
                                                    <tr>
                                                       <th>Organization</th>
                                                       <th>Section</th>
                                                       <th>Client Name</th>
                                                       <th>Client Contact</th>
                                                       <th class="text-center">View Full Info</th>
                                                       <th class="text-center">Status</th>
                                                   </tr>
                                               </tfoot>
                                               <tbody>
                                               @foreach($bookings->all() as $booking)
                                               <tr>
                                                    <td>{{ $booking->organization_name }}</td>
                                                    <td>{{ $booking->item_title }}</td>
                                                    <td>{{ $booking->booked_user_name }}</td>
                                                    <th>{{ $booking->booked_user_phone }}</th>
                                                    <td class="text-center">
                                                        
                                                        <button class="btn btn-sm btn-default" data-toggle="modal" data-target="#deleteModal{{$booking->bookingID}}">View</a>
                                                    </td>
                                                    
                                                    <td class="text-center">
                                                    @if( $booking->bookingStatus == 1 ||  $booking->bookingStatus == 2)
                                                    <a href="{{url('/organization/confirmation/disable/'.$booking->bookingID)}}"  class="btn btn-success btn-sm">Confirm Now</a>
                                                    @elseif( $booking->bookingStatus == 0)
                                                    <a href="{{url('/organization/confirmation/enable/'.$booking->bookingID)}}" class="btn btn-danger btn-sm">Not Confirm</a>
                                                    @endif
                                                    </td>
                                                </tr>
                                                <!-- Modal -->
                                                <div class="modal fade" id="deleteModal{{$booking->bookingID}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">Booking Info</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                            <p><strong>Organization Name: </strong>{{ $booking->organization_name }}</p>
                                                            <p><strong>Section Title: </strong>{{ $booking->item_title }}</p>
                                                            <p><strong>Section Amount: </strong>{{ $booking->booked_item_amount }}</p>
                                                            <p><strong>Booking Date </strong>{{ $booking->booking_date }}</p>
                                                            <p><strong>Booking Time: </strong>{{ $booking->booking_date }}</p>
                                                            <p><strong>Guests: </strong>{{ $booking->booking_guests }}</p>
                                                            <p><strong>Client Name: </strong>{{ $booking->booked_user_name }}</p>
                                                            <p><strong>Client Email: </strong>{{ $booking->booked_user_email }}</p>
                                                            <p><strong>Client Phone: </strong>{{ $booking->booked_user_phone }}</p>
                                                            <p><strong>Client Company: </strong>{{ $booking->booked_user_company }}</p>
                                                            <p><strong>Client Messages: </strong>{{ $booking->booked_user_message }}</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                                @if( $booking->bookingStatus == 1)
                                                                    <a href="{{url('/organization/confirmation/disable/'.$booking->bookingID)}}"  class="btn btn-success">Confirm Now</a>
                                                                 @elseif( $booking->bookingStatus == 0)
                                                                    <a href="{{url('/organization/confirmation/enable/'.$booking->bookingID)}}" class="btn btn-danger btn-sm">Not Confirm</a>      
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Modal Ends-->
                                                @endforeach
                                               </tbody>
                                           </table>
                                       </div>
                                   </div>
           
                               </div>
           
                           </div>
           
                       </section>
                       <!--Section: Basic examples-->
                      
                  
                  
        </div>
    </div>
</div>
<script>toastr.options = {
  "closeButton": false,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": 300,
  "hideDuration": 1000,
  "timeOut": 5000,
  "extendedTimeOut": 1000,
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}
 
</script>

@endsection
