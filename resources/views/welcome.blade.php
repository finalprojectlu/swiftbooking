<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SwiftBooking</title>
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa|Righteous" rel="stylesheet">
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <!-- Material Design Bootstrap -->
    <link href="{{ asset('css/compiled.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">
    <!-- DataTables.net --> 
    <link href="{{ asset('js/vendor/datatables/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <style>

html,
body,
header,
.jarallax {
  height: 700px;
}

@media (max-width: 740px) {
  html,
  body,
  header,
  .jarallax {
    height: 100vh;
  }
}

@media (min-width: 800px) and (max-width: 850px) {
  html,
  body,
  header,
  .jarallax {
    height: 100vh;
  }
}

@media (min-width: 560px) and (max-width: 650px) {
  header .jarallax h1 {
    margin-bottom: .5rem !important;
  }
  header .jarallax h5 {
    margin-bottom: .5rem !important;
  }
}


@media (min-width: 660px) and (max-width: 700px) {
  header .jarallax h1 {
    margin-bottom: 1.5rem !important;
  }
  header .jarallax h5 {
    margin-bottom: 1.5rem !important;
  }
}

.top-nav-collapse {
    background-color: #9da4b1 !important;
}
.navbar:not(.top-nav-collapse) {
    background: transparent !important;
}
@media (max-width: 768px) {
    .navbar:not(.top-nav-collapse) {
        background: #9da4b1 !important;
    }
}
@media (min-width: 800px) and (max-width: 850px) {
    .navbar:not(.top-nav-collapse) {
        background: #9da4b1!important;
    }
}

footer.page-footer {
    background-color: #9da4b1;
}

</style>
<style type="text/css">/* Chart.js */
@-webkit-keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}@keyframes chartjs-render-animation{from{opacity:0.99}to{opacity:1}}.chartjs-render-monitor{-webkit-animation:chartjs-render-animation 0.001s;animation:chartjs-render-animation 0.001s;}</style><style type="text/css" id="#jarallax-clip-0">#jarallax-container-0 {
   clip: rect(0 1349px 613px 0);
   clip: rect(0, 1349px, 613px, 0);
}</style></head>
<body>

    <!--Main Navigation-->
    <header>

        <!--Navbar-->
        <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar aqua-gradient">
            <div class="container">
				<a class="navbar-brand text-" href="#">Swift<strong>Booking</strong></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                        </li>
                        
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Intro Section -->
        <div class="view jarallax" data-jarallax="{&quot;speed&quot;: 0.2}" style="background-image: none; background-repeat: no-repeat; background-size: auto; background-position: center center; z-index: 0; background-attachment: scroll;" data-jarallax-original-styles="background-image: url(&#39;https://mdbootstrap.com/img/Photos/Others/gradient3.png&#39;); background-repeat: no-repeat; background-size: cover; background-position: center center;">
          <div class="mask rgba-purple-slight">
            <div class="container h-100 d-flex justify-content-center align-items-center">
                <div class="row pt-5 mt-3">
                    <div class="col-md-12 wow fadeIn mb-3" style="visibility: visible; animation-name: fadeIn;">
                        <div class="text-center">
                                    <h1 class="display-4 font-weight-bold mb-5 wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">Swift<b>Booking</b><blink>_</blink></h1>
                                    <h5 class="mb-5 wow fadeInUp" data-wow-delay="0.2s" style="visibility: visible; animation-name: fadeInUp; animation-delay: 0.2s;">UNIVERSAL ONLINE BOOKING FOR ALL!</h5>
                                <div class="wow fadeInUp" data-wow-delay="0.4s" style="visibility: visible; animation-name: fadeInUp; animation-delay: 0.4s;">
                                <a href="register"class="btn btn-primary btn-rounded waves-effect waves-light">Sign up</a>
</br>
                                <a href="login"class="btn btn-default btn-rounded waves-effect waves-light"> Sign In</a>
                                    
                                </div>
                            
                        </div>
                    </div>
                </div>
            </div>
          </div>
        <div id="jarallax-container-0" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -100;"><div style="background-position: 50% 50%; background-size: 100%; background-repeat: no-repeat; background-image: url(&quot;https://mdbootstrap.com/img/Photos/Others/gradient3.png&quot;); position: fixed; top: 0px; left: 0px; width: 1349px; height: 899.138px; overflow: hidden; pointer-events: none; margin-left: 0px; margin-top: -143.069px; visibility: visible; transform: translateY(0px) translateZ(0px);"></div></div></div>

    </header>
    <!--Main Navigation-->

    <!--Main Layout-->
    <main>

        <div class="container">

            <!--Section: Features v.4-->
            <section class="section wow fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeIn; animation-delay: 0.3s;">

                <!--Section heading-->
                <h1 class="section-heading h1 pt-4">Why is it so great?</h1>
                <!--Section description-->
                <p class="section-description mb-5 pb-3"></p>

                <!--Grid row-->
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-6">

                        <!--Grid row-->
                        <div class="row mb-6">
                            <div class="col-2">
                                <i class="far fa-2x fa-flag indigo-text"></i>
                            </div>
                            <div class="col-10">
                                <h5 class="font-weight-bold my-4">UNIVERSAL</h5>
                                <p class="grey-text">One click online booking,one can easily book any ticket here.</p>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row mb-6">
                            <div class="col-2">
                                <i class="fas fa-2x fa-flask blue-text"></i>
                            </div>
                            <div class="col-10">
                                <h5 class="font-weight-bold my-4">LIMIT</h5>
                                <p class="grey-text">Owner can set limit for his service.</p>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row mb-6">
                            <div class="col-2">
                                <i class="fa-2x far fa-hourglass cyan-text"></i>
                            </div>
                            <div class="col-10">
                                <h5 class="font-weight-bold my-4">Time</h5>
                                <p class="grey-text">Swift Booking save times and physical work for guest & ownwer both.</p>
                            </div>
                        </div>
                        <!--Grid row-->

                    </div>
                    <!--Grid column-->

                    
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-6">

                        <!--Grid row-->
                        <div class="row mb-6">
                            <div class="col-2">
                                <i class="far fa-2x fa-heart deep-purple-text"></i>
                            </div>
                            <div class="col-10">
                                <h5 class="font-weight-bold my-4">Dynamic</h5>
                                <p class="grey-text">Its dynamic so that it's work efficiently.</p>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row mb-6">
                            <div class="col-2">
                                <i class="fas fa-2x fa-bolt purple-text"></i>
                            </div>
                            <div class="col-10">
                                <h5 class="font-weight-bold my-4">Simple</h5>
                                <p class="grey-text">Too easy to use for all.</p>
                            </div>
                        </div>
                        <!--Grid row-->

                        <!--Grid row-->
                        <div class="row mb-6">
                            <div class="col-2">
                                <i class="fas fa-2x fa-magic purple-text"></i>
                            </div>
                            <div class="col-10">
                                <h5 class="font-weight-bold my-4">Compare</h5>
                                <p class="grey-text">All type of organization is here,so that one can compare easily.</p>
                            </div>
                        </div>
                        <!--Grid row-->

                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

            </section>
            <!--/Section: Features v.4-->

            <hr class="mb-5">

            <!--Section: Testimonials v.3-->
            <section class="section team-section text-center pb-3 wow fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeIn; animation-delay: 0.3s;">

                <!--Section heading-->
                <h1 class="section-heading h1 pt-4">DEVELOPER</h1>
                <!--Section description-->
                <p class="section-description mb-5 pb-3">We have such a good,hard working & energetic team</p>

                <!--Grid row-->
                <div class="row text-center">

                    <!--Grid column-->
                    <div class="col-md-3 mb-3">

                        <div class="testimonial">
                            <!--Avatar-->
                            <div class="avatar mx-auto">
                                <img src="img/rukz.jpg" class="rounded-circle z-depth-1 img-fluid">
                            </div>

                            <!--Content-->
                            <h4 class="font-weight-bold mt-4 mb-3">Abdulla Al Azad</h4>
                            <h6 class="mb-3 font-weight-bold grey-text">Web Developer</h6>
                            

                            <!--Review-->
                            <div class="orange-text">
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star-half-full"> </i>
                            </div>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-3 mb-3">
                        <div class="testimonial">
                            <!--Avatar-->
                            <div class="avatar mx-auto">
                                <img src="img/raj.jpg" class="rounded-circle z-depth-1 img-fluid">
                            </div>

                            <!--Content-->
                            <h4 class="font-weight-bold mt-4 mb-3">Zubaer Haque Raj</h4>
                            <h6 class="mb-3 font-weight-bold grey-text">Web Developer</h6>
                            

                            <!--Review-->
                            <div class="orange-text">
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                            </div>
                        </div>
                    </div>
                    <!--Grid column-->

                    <!--Grid column-->
                    <div class="col-md-3 mb-3">
                        <div class="testimonial">
                            <!--Avatar-->
                            <div class="avatar mx-auto">
                                <img src="img/farzan.jpg" class="rounded-circle z-depth-1 img-fluid">
                            </div>
                            <!--Content-->
                            <h4 class="font-weight-bold mt-4 mb-3">Farzan Faruk</h4>
                            <h6 class="mb-3 font-weight-bold grey-text">UI/UX Designer</h6>
                            

                            <!--Review-->
                            <div class="orange-text">
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star-o"> </i>
                            </div>

                        </div>
                    </div>
					
					<div class="col-md-3 mb-3">
                        <div class="testimonial">
                            <!--Avatar-->
                            <div class="avatar mx-auto">
                                <img src="img/kadu.jpg" class="rounded-circle z-depth-1 img-fluid">
                            </div>
                            <!--Content-->
                            <h4 class="font-weight-bold mt-4 mb-3">Abdul Kadir</h4>
                            <h6 class="mb-3 font-weight-bold grey-text">Web Designer</h6>
                            

                            <!--Review-->
                            <div class="orange-text">
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star"> </i>
                                <i class="far fa-star-o"> </i>
                            </div>

                        </div>
                    </div>
                    <!--Grid column-->

                </div>
                <!--Grid row-->

            </section>
            <!--Section: Testimonials v.3-->

            <hr class="mb-5">

           

            <!--Section: Contact v.2-->
            <section class="section pb-5 wow fadeIn" data-wow-delay="0.3s" style="visibility: visible; animation-name: fadeIn; animation-delay: 0.3s;">

                <!--Section heading-->
                <h2 class="section-heading h1 pt-4">Contact us</h2>
                <!--Section description-->
                
                <div class="row">

                    <!--Grid column-->
                    <div class="col-md-12 col-xl-12">
                        <form>

                            <!--Grid row-->
                            <div class="row">

                                <!--Grid column-->
                                <div class="col-md-6">
                                    <div class="md-form">
                                        <div class="md-form">
                                            <input type="text" id="contact-name" class="form-control">
                                            <label for="contact-name" class="">Your name</label>
                                        </div>
                                    </div>
                                </div>
                                <!--Grid column-->

                                <!--Grid column-->
                                <div class="col-md-6">
                                    <div class="md-form">
                                        <div class="md-form">
                                            <input type="text" id="contact-email" class="form-control">
                                            <label for="contact-email" class="">Your email</label>
                                        </div>
                                    </div>
                                </div>
                                <!--Grid column-->

                            </div>
                            <!--Grid row-->

                            <!--Grid row-->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="md-form">
                                        <input type="text" id="contact-Subject" class="form-control">
                                        <label for="contact-Subject" class="">Subject</label>
                                    </div>
                                </div>
                            </div>
                            <!--Grid row-->

                            <!--Grid row-->
                            <div class="row">

                                <!--Grid column-->
                                <div class="col-md-12">

                                    <div class="md-form">
                                        <textarea type="text" id="contact-message" class="md-textarea form-control" rows="3"></textarea>
                                        <label for="contact-message">Your message</label>
                                    </div>

                                </div>
                            </div>
                            <!--Grid row-->

                        </form>

                        <div class="text-center text-md-left my-4">
                            <a class="btn btn-light-blue btn-rounded waves-effect waves-light">Send</a>
                        </div>
                    </div>
                    <!--Grid column-->

                    
                    <!--Grid column-->

                </div>

            </section>
            <!--Section: Contact v.2-->

        </div>

    </main>
    <!--Main Layout-->


    <!--Footer-->
    <footer class="page-footer pt-4 mt-4   text-center text-md-left mt-5">

        <!--Footer Links-->
        <div class="container">
            <div class="row">

                <!--First column-->
                <div class="col-md-12 text-center">
                    <h5 class="text-uppercase">Team PaperMint </h5>
                    <p></p>
                </div>
                <!--/.First column-->

                <hr class="w-100 clearfix d-md-none">

               
                

            </div>
        </div>

        <hr>

        <div class="container">
            <!--Grid row-->
            <div class="row mb-3">

                <!--First column-->
                <div class="col-md-12">

                    <ul class="list-unstyled d-flex justify-content-center mb-0 py-4 list-inline">
                        <li class="list-inline-item"><a class="p-2 m-2 fa-lg fb-ic"><i class="fab fa-facebook-f white-text fa-lg"> </i></a></li>
                        <li class="list-inline-item"><a class="p-2 m-2 fa-lg tw-ic"><i class="fab fa-twitter white-text fa-lg"> </i></a></li>
                        <li class="list-inline-item"><a class="p-2 m-2 fa-lg gplus-ic"><i class="fab fa-google-plus white-text fa-lg"> </i></a></li>
                        <li class="list-inline-item"><a class="p-2 m-2 fa-lg li-ic"><i class="fab fa-linkedin white-text fa-lg"> </i></a></li>
                        <li class="list-inline-item"><a class="p-2 m-2 fa-lg ins-ic"><i class="fab fa-instagram white-text fa-lg"> </i></a></li>
                        <li class="list-inline-item"><a class="p-2 m-2 fa-lg pin-ic"><i class="fab fa-pinterest white-text fa-lg"> </i></a></li>
                    </ul>

                </div>
                <!--/First column-->
            </div>
            <!--/Grid row-->
        </div>
        <!--/.Footer Links-->

        <!--Copyright-->
        <div class="footer-copyright py-3 text-center">
            <div class="container-fluid">

            </div>
        </div>
        <!--/.Copyright-->

    </footer>
    <!--/.Footer-->

 	<!-- Scripts -->
     <script src="{{ asset('js/app.js') }}"></script>
    <!-- JQuery -->
    <script type="text/javascript" src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{ asset('js/popper.min.js') }}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{ asset('js/mdb.min.js') }}"></script>
    <!-- DataTables.net -->
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/vendor/datatables/js/dataTables.bootstrap4.min.js') }}"></script>
    <script>
    //options select
        $(document).ready(function() {
            $('.mdb-select').material_select();
        });
        
        // SideNav Button Initialization
        $(".button-collapse").sideNav();
        
			var container = document.querySelector('.custom-scrollbar');
			Ps.initialize(container, {
				wheelSpeed: 2,
				wheelPropagation: true,
				minScrollbarLength: 20
			});
	
			$(document).ready(function() {
				$('#datatables').DataTable();
			});
	
			// Material Select Initialization
			$(document).ready(function () {
				$('select[name="datatables_length"]').material_select();
			});
    </script>
	<script>
		new WOW().init();

		// MDB Lightbox Init
		$(function () {
		$("#mdb-lightbox-ui").load("../../mdb-addons/mdb-lightbox-ui.html");
		});
  	</script>
</body>
<!--/.Double navigation-->

        

   
</html>
